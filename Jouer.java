import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.lang.Math;
import jeu.Jeu;
import jeu.Joueur;
import jeu.des.DeNormal;
import jeu.des.Demere;
import jeu.des.De0;
import jeu.devinette.Devinette;
import jeu.exceptions.MethodeDeCalculException;
import jeu.gui.GameUI;
import jeu.exceptions.IllegalParameterException;
import jeu.plateau.Plateau;
import jeu.vecteurs.VectorsMap;

/**
 * La classe qui permet de lancer le jeu Contient uniquement la méthode Main
 * Permet de lancer le jeu avec le paramétrage par défaut ou avec les paramètres
 * entrés par l'utilisateur dans le terminal
 * 
 * @see Jeu
 * @see Joueur
 * @see Plateau
 * @see VectorsMap
 * @see Demere
 * @see De0
 * @see DeNormal
 * @see Devinette
 */

public class Jouer {

    public static void main(String[] args) throws IOException, MethodeDeCalculException, IllegalParameterException {

        FileWriter logWriter = new FileWriter("log.txt");

        if (args.length == 1 && args[0].equals("UI")) {
            GameUI gameUI = new GameUI();
            gameUI.main(null);
        }

        else if (args.length != 0 && !args[0].equals("UI")) {
            // Paramètrage
            Scanner sc = new Scanner(System.in);
            // on s'assure que les paramètres sont entrés correctement
            if ((Math.pow(Math.sqrt(Integer.valueOf(args[1])), 2) != Integer.valueOf(args[1]))
                    || Integer.valueOf(args[5]) > 2 || Integer.valueOf(args[5]) <= 0 || Integer.valueOf(args[6]) > 2
                    || Integer.valueOf(args[6]) <= 0) {
                throw new IllegalParameterException();
            }

            else {
                VectorsMap vecteurs = new VectorsMap(args[0]);
                Plateau plateau = new Plateau(Integer.valueOf(args[1]));
                Joueur[] joueurs = new Joueur[Integer.valueOf(args[2])];
                for (int i = 0; i < joueurs.length; i++) {
                    System.out.println("Veuillez entrer le nom du joueur " + String.valueOf(i + 1));
                    Joueur j = new Joueur(sc.nextLine());
                    joueurs[i] = j;
                }
                int nbTry = Integer.valueOf(args[3]);
                int k = Integer.valueOf(args[4]); // nombre de k-réponse retournés par le système
                int typeDe = Integer.valueOf(args[5]);
                Demere de = new Demere();
                if (typeDe == 1) {
                    de = new De0();
                } else if (typeDe == 2) {
                    de = new DeNormal();
                }
                int calcul = Integer.valueOf(args[6]);
                Devinette devinette = new Devinette(calcul);
                Jeu j = new Jeu(plateau, vecteurs, joueurs, de, devinette, nbTry, k, true, false, logWriter);
                j.jouer();

            }
        } else {
            // Paramètrage par défaut

            // Initialisation d'un plateau fixe de 25 cases
            Plateau plateau = new Plateau(25);

            VectorsMap vecteurs = new VectorsMap("w2v_final3");

            DeNormal deClassique = new DeNormal();

            Devinette devinette = new Devinette(1);

            Joueur j1 = new Joueur("Joueur 1");
            Joueur j2 = new Joueur("Joueur 2");
            Joueur[] joueurs = { j1, j2 };

            Jeu j = new Jeu(plateau, vecteurs, joueurs, deClassique, devinette, 3, 10, true, false, logWriter);

            // logWriter.write(j.toString());
            // logWriter.close();

            j.jouer();

            // fw.write(j.log);

        }
    }
}
