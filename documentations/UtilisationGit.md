## Petit guide d'utilisation de Git

### **Commandes de base**

1. **Add** : ajout de fichier à un commit

   Syntaxe : _git add ._ (Ajoute tous les fichiers modifiés depuis le dernier commit) / _git add nom_fichier_ (Ajouter le fichier nom_fichier)

2. **Commit** : enregistrement d'une nouvelle version du projet

   Syntaxe : _git commit message_ (Enregistre la nouvelle version du projet avec un message (e.g "Ajout de la classe Test.java", "Implémentation de l'interface graphique", etc...))

3. **Push** : envoie de la nouvelle version du projet au remote

   Syntaxe : _git push nom_branche_ (Envoie la nouvelle version du projet sur le dossier distant, hébergé sur un site web)

4. **Checkout** : crée une branche dans le projet OU nous place dans une branche déjà existante (faire un checkout en créant une branche nous place directement dans celle-ci)

   Syntaxe : _git checkout nom_branche_

5. **Merge** : permet de fusionner une branche existante avec une autre branche (généralement on merge une branche de développement vers la branche master)

   Syntaxe : _git merge_

### Vocabulaire

- Un dossier distant est appelé un **repository (ou repo)**
- Une **branche** est une version du projet qui peut être modifiée sans avoir d'impact sur le code "principal"
- Un **conflit** est une portion de code qui a été modifiée dans deux commit "de même niveau"

### Bonnes pratiques

- Il est conseillé de toujours travailler dans une branche (même pour le débug) pour éviter d'avoir des conflits
