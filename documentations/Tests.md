# Tests Geenson Game

Ensemble des tests pour le jeu de Geenson

## Lancer les tests

**D'abord il faut se placer à la racine du projet (dossier java-w2v-game)**

_A noter : cette syntaxe est utilisable pour n'importe quelle classe. Ici, on se concentre seulement sur les tests, qui vont eux-mêmes compiler les classes dont ils ont besoin._

- TestUI.java
  ```
  javac jeu/tests/TestUI.java
  java jeu.tests.TestUI
  ```
- TestTourDeJeu.java
  ```
  javac jeu/tests/TestTourDeJeu.java
  java jeu.tests.TestTourDeJeu
  ```
- TestVecteurs.java
  ```
  javac jeu/tests/TestVecteurs.java
  java jeu.tests.TestVecteurs
  ```
- TestPlateau.java
  ```
  javac jeu/tests/TestPlateau.java
  java jeu.tests.TestPlateau
  ```
