# Tour de jeu

1. **Lancer de dés**

   Utilisation d'un objet Random (possibilité )

2. **Déplacement du joueur**

   Mise à jour de la case

3. **Type de case**

   - Case normale
     - Jeu normal
       - Jeu demande au joueur de lui faire deviner un mot
       - Attente de l'input du joueur
       - Vérification de la conformité de l'entrée
       - Calcule du vecteur associé aux indices entrés par le joueur
       - Calcule de la similarité avec les vecteurs existants et création d'une liste des dix mots les plus similaires
       - Résultat
         - Correct -> Le joueur rejoue (relance le dés)
         - Incorrect -> Tour du joueur suivant
   - Case spéciale
     - Case "relance le dés" -> Le joueur est invité à relancer le dès
     - Case "recule de 3 cases" -> La position du joueur est mise à jour
     - Case "avance de 3 cases" -> La position du joueur est mise à jour

4. **Prochain tour**
