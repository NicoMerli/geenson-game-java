
# Java W2V Game (Jeu de Geenson)

---
Un jeu de plateau codé en Java utilisant la représentation de mots en vecteurs (inspiré du jeu de Geenson)

## Principe du jeu

Les joueurs lancent un dé à tour de rôle et avancent sur un plateau. Le but du jeu est d’arriver le premier à la case finale. A chaque tour, le système propose au joueur un mot m à lui faire deviner.Le joueur doit alors donner un certain nombre d’indices et le système en fonction de ceux-ci retournera les k-réponses qui lui semblent les plus pertinentes.

## Tour de jeu

1. ***Lancer de dés***

   Utilisation d'un objet Random

2. ***Déplacement du joueur***

   - Mise à jour de la case du joueur
   - Attribution du joueur à la nouvelle case
   - Suppression du joueur de la case précédente

3. ***Vérification du type de case***

   - Case normale
   - Case spéciale
     - Case "Recule" -> Le joueur recule de 3 cases
     - Case "Avance" -> Le joueur avance de 2 cases
     - Case "Prison" -> le joueur ne sort pas de prison tant qu'il n'a pas résolu l'énigme; dans le cas où il sort de prison, il ne peut pas rejouer jusqu'au prochain tour, le tour passe au joueur suivant
     - Case "Final" -> le jeu se termine

4. ***Lancement de la devinette***

    - Le jeu demande au joueur de lui faire deviner un mot
    - Attente de l'input du joueur 
    - Vérification de la conformité de l'entrée
    - Calcule du vecteur associé aux indices entrés par le joueur
    - Calcule de la similarité avec les vecteurs existants et création d'une liste des dix mots les plus similaires
    - Résultat
        - Correct -> Le joueur rejoue (il relance le dé)
        - Incorrect -> Tour du joueur suivant

5. ***Prochain tour***

    Répétition des points 1-4

6. ***Fin du jeu***

	Le jeu se termine quand un des joueurs se trouve sur la case de type "Final"

## Création de vecteurs et méthodes de calcul

 1.  ***Création de vecteurs***

- On crée un map de vecteurs à partir du fichier "w2v_final3" par défaut (possibilité de changer de fichier en le rentrant en paramètres lors du lancement du jeu).

- Structure de VectorsMap:
    - clé : mot
    - valeur : vecteurs

2. ***Calcul de la similarité***

	On implémente deux méthodes de calcul :
	 - La similarité cosinus (par défaut)
	 - La distance

## Lancement du programme

Pour lancer le programme et faire l'impression du plateau dans le terminal il faut se déplacer à la racine du projet

***Pour le paramètrage par défaut***

> javac Jouer.java

> java Jouer

*Paramètres par défaut:*

- nombre de cases du plateau : 25
- fichier contenant les mots avec leurs vecteurs : "w2v_final3"
- nombre de joueurs : 2
- nombre d'essais pour résoudre l'énigme : 3
- nombre de mots que le système retourne : 10
- type du dé : dé normal avec 6 faces
- méthode de calcul : similarité cosinus

***Pour lancer le jeu avec vos paramètres***

> javac Jouer.java 

- **args[0]** nombre de cases du plateau (la carré d'un entier)
- **args[1]** nom du fichier contenant les mots avec leurs vecteurs ("w2v_final3" par défaut)
- **args[2]** nombre de joueurs
- **args[3]** nombre d'essais de résoudre l'énigme
- **args[4]** nombre de mots à retourner par le système
- **args[5]** type de dé : à choisir entre la dé normal et le dé magique avec la face 0
- **args[6]** méthode de calcul de la similarité : 
	- 1 pour calcul de la similarité cosinus
	- 2 pour le calcul de la distance

## Interface graphique Swing ##

L'interface graphique Swing n'est pas fonctionnelle. Nous n'avons pas trouvé un moyen efficace de gérer les évènements de la partie backend du déroulement du jeu et de les intégrer à l'interface graphique que nous avons voulu implémenter.

Vous trouverez cependant une version d'essai non terminée dans le dossier */jeu/gui/*.

Pour la compiler et la lancer :

> javac jeu/gui/GameView.java

> javac jeu/gui/Options.java

> java Jouer UI

Nous pensons qu'en ayant utilisé les méthodes de programmation évènementielles dés la conception du jeu, nous ne serions pas heurtés à autant de problèmes de mise en oeuvre.

## La documentation

Pour consulter la javadoc du jeu, il faut lancer le fichier

> /doc/index.html

dans le navigateur

## Auteurs

- Sofiya Kobylyanskaya
- Rim-Itto Loubatieres
- Nicolas Merli
