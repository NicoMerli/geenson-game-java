package jeu.exceptions;

/**
 * Classe qui vérifie que les paramètres entrés par l'utilisateur sont corrects
 * @inheritDoc Exception
 */

public class IllegalParameterException extends Exception {

    /**
     *Constructeur qui crée un message d'erreur
     */
    public IllegalParameterException() {
        System.out.println("Veuillez consulter le readme.md et entrer les paramètres correspondant");
    }
}
