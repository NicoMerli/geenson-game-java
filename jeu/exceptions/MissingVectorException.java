package jeu.exceptions;

/**
 *Classe qui vérifie que le vecteur existe
 *@inheritdoc Exception
 */

public class MissingVectorException extends Exception {

    /**
     *Constructeur qui crée un message d'erreur
     */
    public MissingVectorException() {
        System.out.println("Ce vecteur n'existe pas.");
    }

}
