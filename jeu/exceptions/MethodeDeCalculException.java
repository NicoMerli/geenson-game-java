package jeu.exceptions;

/**
 *Classe qui vérifie que la méthode de calcul est correcte
 *@inheritDoc Exception
 */
public class MethodeDeCalculException extends Exception {

    /**
     * Constructeur qui crée un message d'erreur
     */
    public MethodeDeCalculException() {
        System.out.println(
                "Cette méthode de calcul n'existe pas. tapez \"1 \" pour calculer la similarité cosinus, \"2\" pour la distance.");
    }
}
