package jeu.devinette;

import jeu.vecteurs.VectorsMap;
import jeu.vecteurs.Vector;
import jeu.exceptions.MethodeDeCalculException;

import java.util.*;
import java.util.TreeMap;
import java.util.Random;
import java.util.Scanner;

/**
 *Classe qui implémente la logique de la devinette
 */

public class Devinette {
	static Random rand = new Random();
	static Scanner sc = new Scanner(System.in);

	private String TheWordToFind;
	private String[] listeIndices;
	private boolean devine = false;
	private int methode;

	/**
	 *Constructeur de la classe
	 *@param calcul le méthode de calcul: choix entre 1 - similarité cosunus et 2 - distance
	 *@throws MethodeDeCalculException
	 *@see MethodeDeCalculException
	 */
	public Devinette(int calcul) throws MethodeDeCalculException {
		// On controle dans le constructeur le fait que l'utilisateur donne une methode
		// de calcul qui convient = 1=Similarite cosinus, 2=distance. Si autre chose est
		// indique, une exception est declenchee.
		if (calcul != 1 && calcul != 2) {
			throw new MethodeDeCalculException();
		} else {
			this.methode = calcul;
		}
	}

	/**
	 *Méthode qui met à jour le mot à deviner
	 *@param mot le mot à deviner
	 */
	public void setWord(String mot) {
		this.TheWordToFind = mot;
	}

	/**
	 *Méthode qui renvoie le mot à deviner
	 *@return le mot à deviner
	 */
	public String getWord() {
		return this.TheWordToFind;
	}

	/**
	 *Méthode qui met à jour la liste d'indices
	 *@param tab un tableau d'indices (de string)
	 */
	public void setIndices(String[] tab) {
		this.listeIndices = tab;
	}

	/**
	 *Méthode qui renvoie la liste d'indices
	 *@return la liste d'indice (un tableau de string)
	 */
	public String[] getIndices() {
		return this.listeIndices;
	}

	/**
	 *Méthode qui met à jour le champs d'instance indiquant si le joueur a réussi l'énigme
	 *@param b le booléen indiquant si le joueur a réussi l'énigme
	 */
	public void setDevine(boolean b) {
		this.devine = b;
	}

	/**
	 *Méthode qui renvoie le booléen indiquant si le joueur a réussi l'énigme
	 *@return le booléen indiquant si le joueur a réussi l'énigme
	 */
	public boolean getDevine() {
		return this.devine;
	}

	/**
	 *Méthode qui lance la devinette en fonction de la méthode choisie (distance ou cosinus)
	 *@param vecteurs les mots avec leurs valeurs
	 *@param nbReponses le nombre de réponses que le système doit retourner
	 *@see VectorsMap
	 */
	
	public void question(VectorsMap vecteurs, int nbReponses) {
		if (this.methode == 1) {
			this.FinalFinal(this.FinalCosinus(
					this.calculs_moyenne(this.transformation_vecteurs(this.interactions_joueur(vecteurs), vecteurs)),
					vecteurs, nbReponses));
		} else {
			this.FinalFinal(this.FinalDistance(
					this.calculs_moyenne(this.transformation_vecteurs(this.interactions_joueur(vecteurs), vecteurs)),
					vecteurs, nbReponses));
		}
	}

	/**
	 *Méthode qui lance la devinette dans la version debug
	 */
	public void questionDebug() {
		System.out.println("Version debug de la devinette. Entrez gagne ou perd :");
		String res = sc.nextLine();
		if (res.equals("gagne")) {
			System.out.println("Enigme gagnée");
			this.setDevine(true);
		} else if (res.equals("perd")) {
			System.out.println("Enigme perdue");
			this.setDevine(false);
		} else {
			System.out.println("Commande non-reconnue");
		}
	}

	/**
	 *Méthode qui permet de visualiser les réponses avec leur valeurs
	 *@param tab un tableau de string représentant les réponses
	 *@param t un tableau de double représentant les valeurs
	 */
	
	// trois auxiliaires de test
	public static void printStringArray(String[] tab, Double[] t) {
		// permet de visualiser les réponses avec leur valeurs
		for (int i = 0; i < tab.length; i++) {
			System.out.println(tab[i] + ":" + t[i]);
		}
	}

	/**
	 *Méthode qui imprime le tableau de vecteurs
	 *@param tab le tableau de vecteurs
	 *@see Vector
	 */
	public static void printVectorArray(Vector[] tab) {
		for (int i = 0; i < tab.length; i++) {
			System.out.println(tab[i].toString());
		}
	}

	/**
	 *Méthode qui renvoie le mot à faire deviner à l'ordinateur
	 *@param mots un tableau de mots
	 *@return le mot à faire deviner choisi aléatoirement
	 */
	public String mot_a_deviner(String[] mots) {
		int index = rand.nextInt(mots.length);
		String mot = mots[index];
		System.out.println("vous devez faire deviner a l'ordinateur ce mot : " + mot);
		this.setWord(mot);
		return mot;

	}

	/**
	 *Methode qui entre en interaction avec le joueur, renvoie les indices données par le joueur 
	 *et vérifie que les mots rentrés soient tous présents dans le VectorsMap
	 *@param VM le vectorsMap des mots avec leurs valeurs
	 *@return les indices donnés par l'utilisateur (un tableau de string)
	 *@see VectorsMap
	 */
	public String[] interactions_joueur(VectorsMap VM) {
		// la version terminal de l'interraction joueur, où le joueur va pouvoir donner
		// ses indices et un while qui vérifie un a un les mots jusqu'à ce qu'ils soient
		// tous contenus dans le VectorsMap.
		String[] indices = new String[3];
		System.out.println("veuillez entrer 3 indices");
		for (int i = 0; i < indices.length; i++) {
			indices[i] = sc.next();
			while (!VM.containsKey(indices[i])) {
				System.out.println("Ce mot n'est pas bon. Recommencez!");
				indices[i] = sc.next();
			}
		}
		this.setIndices(indices);
		// printStringArray(indices);
		return indices;
	}

	/**
	 *Méthode qui entre en interaction avec le joueur 
	 *mais ne vérifie par que les mots soient contenus dans le vectormap (version interface)
	 *@return les indices donnés par le joueur(un tableau de string)
	 */
	public String[] interactions_joueur_interface() {
		// La version interface de l'interraction joueur, qui ne vérifie pas que les
		// mots sont bien dans le VectorMap
		String[] indices = new String[3];
		System.out.println("veuillez entrer 3 indices");
		for (int i = 0; i < indices.length; i++) {
			indices[i] = sc.next();
		}
		this.setIndices(indices);
		// printStringArray(indices);
		return indices;
	}

	/**
	 *Méthode qui renvoie un tableau de booléen indiquant si les indices sont contenus dans le vectormap
	 *@param VM le VectorsMap contenant les mots avec leurs valeurs
	 *@return un tableau de booléen indiquant si les indices sont contenus dans le vectormap
	 *@see VectorsMap
	 */
	public boolean[] tab_boolean_interface(VectorsMap VM) {
		// va vérifier que tous les mots sont dans VM, renvoie true ou false en
		// conséquence.
		boolean tab[] = new boolean[3];
		for (int i = 0; i < 3; i++) {
			if (!VM.containsKey(this.listeIndices[i])) {
				tab[i] = false;
			} else {
				tab[i] = true;
			}
		}
		return tab;
	}

	/**
	 *Méthode qui retourne un tableau de vecteurs correspondant aux indices rentrés par le joueur
	 *@param tab le tableau d'indices rentrés par le joueur (taleau de string)
	 *@param VM un vectorsMap représentant l'ensemble de mots et leurs valeurs
	 *@return un tableau de vecteurs correspondant aux indices rentrés par le joueur
	 *@see VectorsMap
	 */
	public Vector[] transformation_vecteurs(String[] tab, VectorsMap VM) {
		Vector[] TabV = new Vector[3];
		for (int i = 0; i < tab.length; i++) {
			TabV[i] = VM.get(tab[i]);
		}
		// printVectorArray(TabV);
		return TabV;
	}

	/**
	 *Méthode qui calcule la moyenne des vecteurs
	 *@param tab un tableau de vecteurs
	 *@return le vecteur représentant la moyenne des vecteurs
	 *@see Vector
	 */
	public Vector calculs_moyenne(Vector[] tab) {
		Vector l = tab[0];
		l = l.moyenne(tab[1], tab[2]);
		// System.out.println(l.toString());
		// System.out.println(l.getValues().length);
		return l;
	}

	/**
	 *Méthode qui renvoie le tableau des mots les plus proches aux indices 
	 *proposés par le joueur en utilisant la méthode du calcul de la distance
	 *@param v vecteur
	 *@param VM vectorsMap de tous les mots avec leurs valeurs
	 *@param nbReponses le nombre de réponses que l'ordinateur doit renvoyer
	 *@return un tableau de mots les plus proches aux indices rentrés par l'utilisateur 
	 *@see Vector
	 *@see VectorsMap
	 */
	public String[] FinalDistance(Vector v, VectorsMap VM, int nbReponses) {
		// la treemap trie toute seule les donnees, c'est une alternative aux
		// algorithmes de tri.
		// On met la valeur en clef, car la treemap trie selon les clefs et non les
		// valeurs
		TreeMap<Double, String> TM = new TreeMap<Double, String>();
		String[] tab = VM.getWords();

		for (int i = 0; i < tab.length; i++) {
			TM.put(v.distance(VM.get(tab[i])), tab[i]);
		}

		// On fait un tableau pour les mots
		String[] mots = TM.values().toArray(new String[TM.size()]);
		String[] Final = new String[nbReponses];

		// et un tableau pour les valeurs
		Set<Double> Set_keys = TM.keySet();
		Double[] Tab_valeurs = Set_keys.toArray(new Double[Set_keys.size()]);
		Double[] valeurs = new Double[nbReponses];

		int k = 0;
		// Nous remplissons les deux tableau dans cette boucle
		for (int i = 0; i < mots.length && k < nbReponses; i++) {

			// nous ne voulons pas que les indices ressortent dans les reponses
			if (!(mots[i].equals(this.getIndices()[0])) && !(mots[i].equals(this.getIndices()[1]))
					&& !(mots[i].equals(this.getIndices()[2]))) {

				Final[k] = mots[i];
				valeurs[k] = Tab_valeurs[i];
				k++;
			}
		}
		printStringArray(Final, valeurs);
		return Final;
	}

	/**
	 *Méthode qui renvoie le tableau des mots les plus proches aux indices 
	 *proposés par le joueur en utilisant la méthode du calcul de cosinus
	 *@param v vecteur
	 *@param VM vectorsMap de tous les mots avec leurs valeurs
	 *@param nbReponses le nombre de réponses que l'ordinateur doit renvoyer
	 *@return un tableau de mots les plus proches aux indices rentrés par l'utilisateur 
	 *@see Vector
	 *@see VectorsMap
	 */
	public String[] FinalCosinus(Vector v, VectorsMap VM, int nbReponses) {
		// la treemap trie toute seule les donnees, c'est une alternative aux
		// algorithmes de tri.
		// On met la valeur en clef, car la treemap trie selon les clefs et non les
		// valeurs
		TreeMap<Double, String> TM = new TreeMap<Double, String>();
		String[] tab = VM.getWords();

		for (int i = 0; i < tab.length; i++) {
			TM.put(v.cosSimilarity(VM.get(tab[i])), tab[i]);
		}

		// On cree un tableau pour les mots
		String[] mots = TM.values().toArray(new String[TM.size()]);
		String[] Final = new String[nbReponses];

		// et un tableau pour les valeurs
		Set<Double> Set_keys = TM.keySet();
		Double[] Tab_valeurs = Set_keys.toArray(new Double[Set_keys.size()]);
		Double[] valeurs = new Double[nbReponses];

		int k = 0;
		// Dans cette boucle, nous remplissons les deux tableaux
		for (int i = mots.length - 1; i > -1 && k < nbReponses; i--) {
			// nous ne voulons pas que les indices ressortent dans les reponses
			if (!(mots[i].equals(this.getIndices()[0])) && !(mots[i].equals(this.getIndices()[1]))
					&& !(mots[i].equals(this.getIndices()[2]))) {
				Final[k] = mots[i];
				valeurs[k] = Tab_valeurs[i];
				k++;
			}
		}
		printStringArray(Final, valeurs);
		return Final;
	}

	/**
	 *Méthode qui vérifie si le joueur a gagné la manche et 
	 *met à jour le champs d'instance devine à true ou à false et imprime le message correspondant
	 *@param tab un tableau de string représentant les réponses retournées par l'ordinateur
	 */
	public void FinalFinal(String[] tab) {
		for (int i = 0; i < tab.length; i++) {
			if (tab[i].equals(this.getWord())) {
				System.out.println("vous avez gagné cette manche");
				this.setDevine(true);
			}
		}
		if (!this.devine)
			System.out.println("Vous n'avez pas remporté cette manche");
	}

}
