package jeu.des;
import java.util.Random;

    /**
     *Classe représentant le dé
     *
     *
     */
    
public class Demere {
    
    static Random rand = new Random();

    /** 
     *Constructeur de la classe qui crée un nouveau de
     */
    
    public Demere() {
    }

    /**
     *Méthode permettant au joueur de lancer le dé
     *@return un nombre aléatoire entre 1 et 6 inclus
     */
    
    public int lancer() {
        return (rand.nextInt(6)) + 1;
    }

}
