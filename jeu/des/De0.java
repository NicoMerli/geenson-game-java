package jeu.des;
import java.util.Random;

    /**
     *Classe représentant un dé magique
     *@inheritDoc Demere
     *@see Demere
     */
public class De0 extends Demere {
    
    static Random rand = new Random();

    /**
     *Constructeur qui crée un nouveau dé magique
     */
    public De0() {
    }

    /**
     *Méthode redéfinie de la classe supérieure Demere
     *renvoie un nombre aléatoire entre 0 et 6 inclus
     *@return un nombre aléatoire entre 0 et 6 inclus
     *{@inheritDoc}
     *@see Demere#lancer()
     */
    @Override
    public int lancer() {
        // la difference, c'est que ce random peut tomber sur 0, jusqu'a 6. 0=avance de
        // zero
        return (rand.nextInt(7));
    }

    public static void main(String[] args) {
    }
}
