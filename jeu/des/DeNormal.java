package jeu.des;

import java.util.Random;

    /**
     *Classe représentant un dé normal
     *@inheritDoc Demere
     *@see Demere
     */

public class DeNormal extends Demere {
    static Random rand = new Random();

    /**
     *Constructeur qui crée un nouveau dé normal
     */
    public DeNormal() {
    }

    /**
     *Méthode redéfinie de la classe supérieure Demere
     *renvoie un nombre aléatoire entre 1 et 6 inclus
     *@return un nombre aléatoire entre 1 et 6 inclus
     *{@inheritDoc}
     *@see Demere#lancer()
     */
    @Override
    public int lancer() {
        return (rand.nextInt(6)) + 1;
    }

    public static void main(String[] args) {
        Demere d = new Demere();
        if (args[0].equals("true")) {
            d = new De0();
        } else {
            d = new DeNormal();
        }
        System.out.println(d.lancer());
    }
}
