package jeu.tests;

import javax.swing.SwingUtilities;

import jeu.gui.affichage.Frame;

public class TestUI {

    /** Exemple de gestion d'une interface graphique basique */

    public static void main(String[] args) {

        // Utilisation du thread dédié de Swing pour la gestion de l'UI
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Frame("Geenson Game", 800, 800);
            }
        });
    }
}