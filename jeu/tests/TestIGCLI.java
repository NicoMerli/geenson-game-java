package jeu.tests;

import jeu.plateau.Plateau;
import jeu.plateau.cases.Case;

public class TestIGCLI {

    public static void printCase(Case c) {
        if (c.getCaseId() < 9) {
            // System.out.print("|" + c.printTypeCLI() + " " + c.printJoueursCLI() + " " +
            // (c.getCaseId() + 1));
            System.out.print("|" + c.printTypeCLI() + " " + "J1" + "  " + (c.getCaseId() + 1));
        } else {
            // System.out.print("|" + c.printTypeCLI() + " " + c.printJoueursCLI() + " " +
            // (c.getCaseId() + 1));
            System.out.print("|" + c.printTypeCLI() + " " + "J2" + " " + (c.getCaseId() + 1));
        }

    }

    public static void main(String[] args) {
        Plateau p = new Plateau(25);

        System.out.println("Map du plateau :\n\n" + p.printPlateauCLI());

        for (Case c : p.getCases()) {
            if ((c.getCaseId() + 1) % Math.sqrt(p.getCases().length) == 0) {
                printCase(c);
                System.out.println("|");
            } else {
                printCase(c);
            }
        }

    }
}