package jeu.tests;

import java.util.Random;

import jeu.Joueur;
import jeu.plateau.Plateau;

public class TestTourDeJeu {

    /** Exemple d'éxecution du jeu de Geenson. */

    public static void main(String[] args) {
        // Initialisation de la partie

        // 1 - Joueurs
        Joueur j1 = new Joueur("Paul");
        Joueur j2 = new Joueur("Jean");

        // 2 - Plateau
        Plateau plateau = new Plateau(20);
        // for (Case c : plateau) {
        // System.out.println(c);
        // }

        // 3 - Placement des joueurs
        j1.setCase(plateau.getCases()[0]);
        j2.setCase(plateau.getCases()[0]);

        // 4 - Dés
        Random des = new Random(); // A remplacer les dès

        // Début de la partie

        // 1 - Présentation
        System.out.println("Début de la partie");

        // 2 - Comptage des joueurs
        // Comptage des joueurs
        Joueur[] joueurs = { j1, j2 };

        boolean isOver = false;
        // 3 - Boucle while
        while (!isOver) {
            for (Joueur j : joueurs) {

                // Lancer de dés
                int move = des.nextInt(6) + 1;
                System.out.println("Le joueur " + j.getName() + " avance de " + move + " cases");

                // Mise à jour de la position
                if (j.getCase().getCaseId() + move >= plateau.getCases().length) {
                    j.setCase(plateau.getCases()[plateau.getCases().length - 1]);
                } else {
                    j.setCase(plateau.getCases()[j.getCase().getCaseId() + move]);
                }
                System.out.println("Nouvelle position : " + j.getCase().getCaseId());

                // Vérification du type de la nouvelle case
                System.out.println(
                        "Le joueur " + j.getName() + " se trouve sur une case de type " + j.getCase().toString());
                if (j.getCase().isFinale()) {
                    isOver = true;
                    break;
                }

                // Question
                System.out.println("Question");

                // Fin de tour
                System.out.println("Fin du tour du joueur " + j.getName());
            }
        }

        // Fin de la partie et détermination du joueur gagnant
        System.out.println("Partie terminée.");
        for (Joueur j : joueurs) {
            if (j.getCase().isFinale()) {
                System.out.println(j.getName() + " remporte la partie !");
            }
        }
    }
}