package jeu.tests;

import java.io.IOException;

// import jeu.devinette.Paires;
import jeu.devinette.Devinette;
import jeu.vecteurs.VectorsMap;
import jeu.exceptions.MissingVectorException;
import jeu.exceptions.MethodeDeCalculException;

public class TestDevinette {

	public static void main(String[] args) throws IOException, MissingVectorException, MethodeDeCalculException {
		VectorsMap vecteurs = new VectorsMap("w2v_final3");
		Devinette Dev = new Devinette(9);
		Dev.mot_a_deviner(vecteurs.getWords());
		Dev.question(vecteurs, 10);
	}

}
