package jeu.tests;

import java.io.IOException;
import java.util.ArrayList;

import jeu.Joueur;
import jeu.plateau.Plateau;

public class TestCase {
	public static void main(String[] args) throws IOException {
		Plateau p = new Plateau(25);
		Joueur j1 = new Joueur("Paul");
		Joueur j2 = new Joueur("Marie");
		Joueur[] joueurs = { j1, j2 };
		ArrayList<Joueur> j = new ArrayList<Joueur>();
		j.add(j1);
		j.add(j2);
		// p.getCases()[5].setJoueurs(j);
		p.getCases()[0].setJoueurs(joueurs);
		p.printPlateau(joueurs.length);
		// p.getCases()[5].getJoueurs().remove(j1);
		p.getCases()[0].removeJoueur(j1);
		p.printPlateau(joueurs.length);
		p.getCases()[0].addJoueur(j1);
		p.printPlateau(joueurs.length);
		// ArrayList <Joueur> jou = new ArrayList<Joueur>();
		// jou = p.getCases()[9].getJoueurs();
		// jou.add(j1);
		// p.getCases()[9].getJoueurs().add(j1);
		// .add(j1);
		// p.printPlateau(joueurs.length);
		// System.out.println(p.getCases()[5].getJoueur("Marie").getName());
		// System.out.println(p.getCases()[5].getJoueur("Paul"));
		// System.out.println(p.getCases()[5].getJoueur(2));
	}
}
