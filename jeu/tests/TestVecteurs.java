package jeu.tests;

import java.io.IOException;
import java.util.Scanner;

import jeu.exceptions.MissingVectorException;
import jeu.vecteurs.Vector;
import jeu.vecteurs.VectorsMap;

public class TestVecteurs {

    /** Batterie de tests pour les vecteurs */
    public static void main(String[] args) throws IOException, MissingVectorException {
        // Instanciation du fichier de vecteurs
        VectorsMap vecteurs = new VectorsMap("w2v_final3");

        // Impression des clés du dictionnaire de vecteurs
        System.out.println("Keyset de " + vecteurs.toString() + ":");
        // for (String label : vecteurs.keySet()) {
        // System.out.println(label);
        // }

        // Impression de la liste des clés de la Map de vecteurs
        for (String label : vecteurs.getWords()) {
            System.out.println(label);
        }

        // Recherche de vecteurs dans une VectorsMap
        Scanner sc = new Scanner(System.in);
        System.out.println("Entrez un terme à rechercher dans la VectorsMap");
        String search = sc.next();
        System.out.println("Recherche du vecteur " + search);
        Vector search_result = vecteurs.get(search);
        if (search_result == null) {
            sc.close();
            throw new MissingVectorException();
        } else {
            System.out.println(vecteurs.get(search).toString());
            sc.close();
        }
    }
}