package jeu.tests;

import jeu.plateau.Plateau;
import jeu.plateau.cases.Case;

public class TestPlateau {
    public static void main(String[] args) {
        Plateau p = new Plateau(20);

        for (Case c : p) {
            System.out.println(c);
        }
    }
}