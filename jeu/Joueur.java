package jeu;

import jeu.plateau.cases.Case;

    /**
     * Classe représentant le joueur
     */
	
public class Joueur {
	
    /**
     *Constructeur de la classe
     */
    	
    /**
     * Un Int représentant le numéro d'identification du joueur 
     * mis à jour à la création d'une nouvelle instance
     */
    private static int id = 0;
    private int playerId;
     /**
      * Un String représentant le nom du joueur
      */
    private String name;
     /**
      * Une Case représentant la position actuelle du joueur sur le plateau
      * @see Case
      */
    private Case position;
     /**
      *Un booléen indiquant si le joueur se trouve sur la case Prison
      *@see CasePrison
      */
    private boolean isInPrison = false;
     /**
      *Un booléen indiquant si le joueur a gagné la manche et s'il peut rejouer
      */
    private boolean gagneManche = true;

    /**
     *Un booléen indiquant si le joueur peut rejouer ou s'il doit attendre le prochain tour
     */
    
    private boolean peutRejouer = true;
    
    /**
     *Crée un joueur avec le nom spécifié
     *@param name String représentant le nom du joueur
     */
    public Joueur(String name) {
        this.name = name;
        this.playerId = id;
        id++;
    }

     /**
      *Renvoie la case sur laquelle se trouve le joueur
      *@return la position du joueur
      *@see Case
      */
    public Case getCase() {
        return this.position;
    }

     /**
      *Renvoie le nom du joueur
      *@return le nom du joueur
      */
    public String getName() {
        return this.name;
    }

     /**
      *Renvoie l'ID du joueur
      *@return ID du joueur
      */
    public int getPlayerId() {
    	return this.playerId;
    }
    
     /**
      *Renvoie le booléen indiquant si le joueur a gagné la manche
      *@return booléen indiquant si le joueur a gagné la manche
      */
    public boolean getGagneManche() {
        return this.gagneManche;
    }

     /**
      *Met à jour le booléen indiquant si le joueur a gagné la manche
      *@param g booléen indiquant si le joueur a gagné la manche
      */
    public void setGagneManche(boolean g) {
        this.gagneManche = g;
    }

     /**
      *Renvoie le booléen indiquant si le joueur se trouve sur la case Prison
      *@return boolean indiquant si le joueur se trouve sur la case Prison
      *@see Case
      */ 
    public boolean getIsInPrison() {
        return this.isInPrison;
    }

     /**
      *Met à jour le booléen indiquant si le joueur se trouve sur la case Prison
      *@param p booléen indiquant si le joueur se trouve sur la case Prison
      *@see Case
      */
    public void setIsInPrison(boolean p) {
        this.isInPrison = p;
    }


     /**
      *Renvoie le booléen indiquant si le joueur peut rejouer ou s'il doit attendre le prochain tour
      *@return booléen indiquant si le joueur peut rejouer ou s'il doit attendre le prochain tour
      */
     
     public boolean getPeutRejouer() {
     	return this.peutRejouer;
     }
     
     /**
      *Met à jour le booléen indiquant si le joueur peut rejouer ou s'il doit attendre le prochain tour
      *@param p boolean
      */
     
     public void setPeutRejouer(boolean p) {
     	this.peutRejouer = p;
     }
     
     /**
      *Met à jour la case sur laquelle se trouve le joueur
      *@param c indiquant la nouvelle case sur laquelle se trouve le joueur
      *@see Case
      */
    public void setCase(Case c) {
        this.position = c;
    }
}
