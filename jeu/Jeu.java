package jeu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import jeu.des.Demere;
import jeu.devinette.Devinette;
import jeu.plateau.Plateau;
import jeu.vecteurs.VectorsMap;
import jeu.plateau.cases.*;
import java.io.IOException;

/**
 * Classe gérant la logique du jeu
 */
public class Jeu {

    private Plateau plateau;
    private Joueur[] joueurs;
    private VectorsMap vecteurs;
    private Demere de;
    private boolean isOver;
    private int nbEssais;
    private int nbReponses;
    private boolean debug;
    private boolean logging;
    private Devinette devinette;
    private int nbJoueurs;
    public String log;
    public FileWriter logWriter;

    /**
     * Constructeur
     * 
     * @param plateau
     * @param vecteurs
     * @param joueurs
     * @param de         - de type De0 ou deNormal
     * @param devinette
     * @param nbEssais   - indique le nombre d'essais que le joueur a pour faire
     *                   deviner le mot à l'ordinateur
     * @param nbReponses - indique le nombre de réponses retournées par l'ordinateur
     * @param logging
     * @param debug
     * @see Plateau
     * @see VectorsMap
     * @see Joueur
     * @see Demere
     * @see De0
     * @see DeNormal
     */

    public Jeu(Plateau plateau, VectorsMap vecteurs, Joueur[] joueurs, Demere de, Devinette devinette, int nbEssais,
            int nbReponses, boolean logging, boolean debug, FileWriter logWriter) {
        this.plateau = plateau;
        this.vecteurs = vecteurs;
        this.joueurs = joueurs;
        this.de = de;
        this.isOver = false;
        this.nbEssais = nbEssais;
        this.nbReponses = nbReponses;
        this.debug = debug;
        this.logging = logging;
        this.devinette = devinette;
        this.nbJoueurs = this.joueurs.length;
        this.logWriter = logWriter;
    }

    /**
     * Renvoie le tableau de joueurs
     * 
     * @return le tableau de joueurs
     * @see Joueur
     */
    public Joueur[] getJoueurs() {
        return this.joueurs;
    }

    /**
     * Renvoie le dé de la partie
     * 
     * @return de le dé de la partie
     * @see Demere
     */
    public Demere getDe() {
        return this.de;
    }

    /**
     * Renvoie le plateau
     * 
     * @return le plateau
     * @see Plateau
     */
    public Plateau getPlateau() {
        return this.plateau;
    }

    public void printConfig(boolean debug) {
        System.out.println(this);
    }

    /**
     * Renvoie une représentation textuelle de la configuration de la partie
     * 
     * @return une représentation textuelle de la configuration de la partie
     */

    public String toString() {
        /**
         * Logging Renvoie une représentation textuelle de la configuration de la partie
         */
        return "Partie initialisée avec les paramètres suivants :\n - Nombre de joueurs : " + joueurs.length
                + "\n - Plateau de " + plateau.getCases().length + " cases\n" + " - Type dé : "
                + this.de.getClass().getName() + "\n - Nombre d'essais : " + this.nbEssais
                + "\n - Réponses autorisées : " + this.nbReponses + "\n - Taille du dictionnaire : "
                + this.vecteurs.size() + "\n - Configuration du plateau : \n" + this.plateau.printPlateauCLI();
    }

    /**
     * Méthode principale du jeu implémentant la logique
     * 
     * @throws IOException
     * @throws ExistingPlayer
     * @throws IllegalIndex
     * @see ExistingPlayer
     * @see IllegalIndex
     */
    public void jouer() throws IOException {

        Scanner sc = new Scanner(System.in);

        // Impression de la config du jeu si lancé en mode debug
        this.printConfig(this.logging);

        if (this.logging) {
            logWriter.write(this.toString() + "\n");
            logWriter.write("Lancement de la partie :\n\n");
        }

        // Placement des joueurs au début du plateau
        for (Joueur j : joueurs) {
            j.setCase(plateau.getCases()[0]);
        }

        // Attribution des joueurs à la case initiale
        this.getPlateau().getCases()[0].setJoueurs(joueurs);

        // Print du plateau initialisé avec les joueurs
        this.getPlateau().printPlateau(nbJoueurs);

        // Tant qu'aucun joueur n'est arrivé à la fin du plateau, on boucle
        while (!this.endGame()) {

            for (Joueur j : joueurs) {

                // Arrêt du forloop si un joueur est si la case arrivée
                if (this.endGame())
                    break;

                if (this.logging) {
                    logWriter.write("\nTour du joueur " + j.getName() + "\n");
                }

                System.out.println("\nTour du joueur " + j.getName());

                if (j.getGagneManche() && j.getPeutRejouer()) { //si le joueur a gagné la manche et s'il peut rejouer, il peut relancer le dé

                    // Lancer de dés
                    System.out.println("Lancez le dé!");
                    sc.nextLine();
                    int move = de.lancer();
                    if (this.logging)
                        logWriter.write("Le joueur " + j.getName() + " lance le dé et fait un " + move + "\n");
                    System.out.println("Le joueur " + j.getName() + " lance le dé et fait un " + move);

                    // Déplacement du joueur
                    move(j, move);

                    // Print du plateau avec la nouvelle position
                    this.getPlateau().printPlateau(nbJoueurs);

                    // Vérification du type de la nouvelle case
                    checkCase(j);

                    // Enigme
                    devine(j);

                    // Vérification si le joueur était en prison
                    if (j.getIsInPrison() && this.devinette.getDevine()) { // si le joueur devine, il sort de prison
                        System.out.println("Le joueur " + j.getName() + " peut sortir de prison");
                        j.setIsInPrison(false);
                        if (this.logging)
                            logWriter.write("Le joueur " + j.getName() + " peut sortir de prison\n");
                    }
                } else {

                    if (j.getIsInPrison()) { // si le joueur est en prison, il peut lancer le dé, mais ne peut pas
                                             // bouger;
                        // Lancer de dés
                        System.out.println("Lancez le dé et faites un six pour sortir de prison !");
                        sc.nextLine();
                        int move = de.lancer();
                        if (this.logging)
                            logWriter.write("Le joueur " + j.getName() + " lance le dé et fait un " + move + "\n");
                        System.out.println("Le joueur " + j.getName() + " lance le dé et fait un " + move);
                        if (move == 6) { // si le joueur fait un 6, il peut sortir de prison
                            System.out.println("Le joueur " + j.getName() + " peut sortir de prison");
                            j.setIsInPrison(false);
                            j.setPeutRejouer(false);
                            if (this.logging)
                                logWriter.write("Le joueur " + j.getName() + " peut sortir de prison\n");
                        } else {
                            System.out.println("Le joueur " + j.getName() + " reste en prison");
                            if (this.logging)
                                logWriter.write("Le joueur " + j.getName() + " reste en prison\n");
                        }
                    }

                    // Print du plateau avec la nouvelle position
                    this.getPlateau().printPlateau(nbJoueurs);

                    devine(j);

                    // Vérification si le joueur était en prison
                    if (j.getIsInPrison() && this.devinette.getDevine()) { // si le joueur devine, il sort de prison
                        System.out.println("Le joueur " + j.getName() + " peut sortir de prison");
                        j.setIsInPrison(false); //le joueur sort de prison, mais ne peut pas rejouer jusqu'au prochain tour
                        if (this.logging)
                            logWriter.write("Le joueur " + j.getName() + " peut sortir de prison\n");
                    }
                }

                while (j.getGagneManche() == true && j.getPeutRejouer()) {
                    // Lancer de dés
                    System.out.println("Lancez le dé!");
                    sc.nextLine();
                    int move = de.lancer();
                    if (this.logging)
                        logWriter.write("Le joueur " + j.getName() + " lance le dé et fait un " + move + "\n");
                    System.out.println("Le joueur " + j.getName() + " lance le dé et fait un " + move);

                    // Déplacement du joueur
                    move(j, move);

                    // Vérification du type de la nouvelle case
                    checkCase(j);

                    if (this.endGame()) { // le jeu est terminé si le joueur est sur la case Arrivée
                        break;
                    }

                    // Print du plateau avec la nouvelle position
                    this.getPlateau().printPlateau(nbJoueurs);

                    // Devinette
                    devine(j);
                }
                
                if (!j.getIsInPrison()) {
                	j.setPeutRejouer(true);
                }
            }
        }

        // Impression du joueur gagnant en mode debug
        if (this.logging) {
            logWriter.write("\nPartie terminée\n");
            for (Joueur j : joueurs) {
                if (j.getCase() instanceof CaseArrivee) {
                    logWriter.write("Le joueur " + j.getName() + " a remporté la partie !\n");
                }
            }
        }
        System.out.println("\nPartie terminée");
        for (Joueur j : joueurs) {
            if (j.getCase() instanceof CaseArrivee) {
                System.out.println("Le joueur " + j.getName() + " a remporté la partie !");
            }
        }

        logWriter.close();
    }

    /**
     * Vérifie la case sur laquelle se trouve un joueur et applique la logique
     * correspondante
     */
    private void checkCase(Joueur j) throws IOException {

        if (this.logging)
            logWriter.write(
                    "Le joueur " + j.getName() + " se trouve sur une case de type " + j.getCase().toString() + "\n");
        System.out.println("Le joueur " + j.getName() + " se trouve sur une case de type " + j.getCase().toString());

        if (j.getCase() instanceof CaseArrivee) {

            this.isOver = true; // Inutile$

        }

        else if (j.getCase() instanceof CaseAvance) {

            if (this.debug)
                logWriter.write("Le joueur " + j.getName() + " avance de 2 cases\n");
            System.out.println("Le joueur " + j.getName() + " avance de 2 cases");
            j.setIsInPrison(false);
            plateau.printPlateau(nbJoueurs);
            j.getCase().effect(plateau, j);
            checkCase(j);

        } else if (j.getCase() instanceof CasePrison) {

            if (this.logging)
                logWriter.write("Le joueur " + j.getName() + " est emprisonné\n");
            System.out.println("Le joueur " + j.getName() + " est emprisonné");
            j.getCase().effect(plateau, j);

        } else if (j.getCase() instanceof CaseRecule) {

            if (this.logging)
                logWriter.write("Le joueur " + j.getName() + " recule de 3 cases\n");
            System.out.println("Le joueur " + j.getName() + " recule de 3 cases");
            j.setIsInPrison(false);
            plateau.printPlateau(nbJoueurs);
            j.getCase().effect(plateau, j);
            checkCase(j);
        }
        else {
        	j.setIsInPrison(false);
        }
    }

    /** Lance la devinette */
    private void devine(Joueur j) throws IOException {

        if (this.debug) {
            devinette.questionDebug();
            if (this.devinette.getDevine()) {
                j.setGagneManche(true);
                if (this.logging)
                    logWriter.write("Enigme remportée\n");
            } else {
                j.setGagneManche(false);
                if (this.logging)
                    logWriter.write("Enigme manquée\n");
            }
        }

        else {
            devinette.setDevine(false);
            if (this.logging)
                logWriter.write("Enigme pour le joueur " + j.getName() + "\n");
            System.out.println("Enigme pour le joueur " + j.getName());

            // Le joueur a un nbEssais de possibilité pour deviner le mot; il peut
            // rejouer s'il réussit l'énigme (on met à jour son champs d'instance gagneMache
            // ce qui lui permet de relancer le dé
            System.out.println();
            this.devinette.mot_a_deviner(vecteurs.getWords());
            for (int i = 0; i < this.nbEssais && !this.devinette.getDevine(); i++) {
                if (this.logging)
                    logWriter.write("Essai " + i + 1 + "\n");
                devinette.question(this.vecteurs, this.nbReponses);
                if (this.devinette.getDevine()) {
                    j.setGagneManche(true);
                    if (this.logging)
                        logWriter.write("Enigme remportée\n");
                    break;
                }
            }
            if (!this.devinette.getDevine()) {
                j.setGagneManche(false);
                if (this.logging)
                    logWriter.write("Enigme manquée\n");
            }

        }
    }

    /** Bouge le joueur sur la case correspondante */
    private void move(Joueur j, int move) {

        if (j.getCase().getCaseId() + move >= plateau.getCases().length) {
            j.setCase(plateau.getCases()[plateau.getCases().length - 1]);
        } else {
            j.setCase(plateau.getCases()[j.getCase().getCaseId() + move]); // déplacement du joueur
            plateau.getCases()[j.getCase().getCaseId() - move].removeJoueur(j); // on enlève le joueur de la case
                                                                                // précédente;
            j.getCase().addJoueur(j); // attribution du joueur à la case correspondante
        }
    }

    /** Renvoie un booléen vérifiant si un joueur est sur la case arrivée */
    public boolean endGame() {

        boolean res = false;
        for (Joueur j : this.joueurs) {
            if (j.getCase() instanceof CaseArrivee) {
                res = true;
                break;
            }
        }
        return res;
    }
}
