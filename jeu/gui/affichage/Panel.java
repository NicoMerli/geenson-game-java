package jeu.gui.affichage;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Panel extends JPanel {

    Frame frame;

    public Panel(Frame frame) {
        this.frame = frame;
    }

    public void paintComponent(Graphics g) {

        // Propriétés du texte
        g.setColor(Color.BLACK);
        FontMetrics fm = g.getFontMetrics();

        // Premier texte
        String text = "Geenson Game";
        g.drawString(text, (frame.width - fm.stringWidth(text)) / 2, 50);

        // Deuxième texte
        String text2 = "C'est très moche pour l'instant et pas rempli mais ça va venir !";
        g.drawString(text2, (frame.width - fm.stringWidth(text2)) / 2, frame.height / 2);
    }

}