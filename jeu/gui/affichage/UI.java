package jeu.gui.affichage;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;

import jeu.Jeu;
import jeu.Joueur;
import jeu.des.De0;
import jeu.des.DeNormal;
import jeu.des.Demere;
import jeu.devinette.Devinette;
import jeu.exceptions.MethodeDeCalculException;
import jeu.plateau.Plateau;
import jeu.plateau.cases.Case;
import jeu.plateau.cases.CaseArrivee;
import jeu.plateau.cases.CaseAvance;
import jeu.plateau.cases.CaseDepart;
import jeu.plateau.cases.CasePrison;
import jeu.plateau.cases.CaseRecule;
import jeu.vecteurs.VectorsMap;

public class UI {

    private MouseListener backClick = new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
            try {
                backMainMenu();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.out.println("Back to main menu"); // Retour au menu principal
        }
    };

    private JFrame frmGeensonGame;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    UI window = new UI();
                    window.frmGeensonGame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public UI() {
        getMainMenu();
    }

    private void getGame(Jeu j) {
        // System.out.println(j);
        frmGeensonGame.getContentPane().removeAll();
        frmGeensonGame.setBounds(100, 100, 1000, 600);
        frmGeensonGame.getContentPane().setLayout(null);

        JPanel logPanel = new JPanel();
        logPanel.setBounds(800, 0, 200, 568);
        frmGeensonGame.getContentPane().add(logPanel);

        JTextArea txtPanelLog = new JTextArea();
        txtPanelLog.setEditable(false);

        JScrollPane scrollPane = new JScrollPane(txtPanelLog);
        scrollPane.setPreferredSize(new Dimension(200, 550));
        logPanel.add(scrollPane);

        String log = "";
        log += "Début de la partie\n";
        txtPanelLog.setText(log);
        log += j.toString();
        txtPanelLog.setText(log);

        JPanel sidePanel = new JPanel();
        sidePanel.setBorder(new LineBorder(new Color(0, 0, 0)));
        sidePanel.setBounds(0, 0, 223, 572);
        frmGeensonGame.getContentPane().add(sidePanel);
        sidePanel.setLayout(null);

        JPanel dicePanel = new JPanel();
        dicePanel.setBackground(Color.DARK_GRAY);
        dicePanel.setBounds(12, 429, 200, 134);
        sidePanel.add(dicePanel);
        GridBagLayout gbl_dicePanel = new GridBagLayout();
        gbl_dicePanel.rowHeights = new int[] { 10, 16, 60 };
        gbl_dicePanel.columnWidths = new int[] { 50, 100, 50 };
        gbl_dicePanel.columnWeights = new double[] { 0.0, 0.0, 0.0 };
        gbl_dicePanel.rowWeights = new double[] { 0.0, 0.0, 0.0 };
        dicePanel.setLayout(gbl_dicePanel);

        // Random rand = new Random();

        JButton btnLancerLeD = new JButton("Lancer le dé");
        GridBagConstraints gbc_btnLancerLeD = new GridBagConstraints();
        gbc_btnLancerLeD.insets = new Insets(0, 0, 5, 5);
        gbc_btnLancerLeD.gridx = 1;
        gbc_btnLancerLeD.gridy = 0;
        dicePanel.add(btnLancerLeD, gbc_btnLancerLeD);

        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setForeground(Color.WHITE);
        lblNewLabel.setBackground(new Color(238, 238, 238));
        lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 60));
        GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
        gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel.gridx = 1;
        gbc_lblNewLabel.gridy = 2;
        dicePanel.add(lblNewLabel, gbc_lblNewLabel);

        btnLancerLeD.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String diceRoll = Integer.toString(j.getDe().lancer());
                // System.out.println(diceRoll);
                lblNewLabel.setText(diceRoll);
            }
        });

        JPanel playerPanel = new JPanel();
        playerPanel.setBounds(12, 12, 199, 30);
        sidePanel.add(playerPanel);
        playerPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        JLabel lblTourDeJoueur = new JLabel("Tour de playerName");
        playerPanel.add(lblTourDeJoueur);

        JPanel enigmePanel = new JPanel();
        enigmePanel.setBackground(Color.GRAY);
        enigmePanel.setBounds(0, 54, 223, 363);
        sidePanel.add(enigmePanel);
        GridBagLayout gbl_enigmePanel = new GridBagLayout();
        gbl_enigmePanel.columnWidths = new int[] { 50, 100, 50 };
        gbl_enigmePanel.rowHeights = new int[] { 20, 0, 0, 0, 0, 0, 30, 0, 0, 30 };
        gbl_enigmePanel.columnWeights = new double[] { 0.0, 1.0, 0.0 };
        gbl_enigmePanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
        enigmePanel.setLayout(gbl_enigmePanel);

        JLabel lblMotDeviner = new JLabel("Mot à deviner");
        GridBagConstraints gbc_lblMotDeviner = new GridBagConstraints();
        gbc_lblMotDeviner.insets = new Insets(0, 0, 5, 5);
        gbc_lblMotDeviner.gridx = 1;
        gbc_lblMotDeviner.gridy = 1;
        enigmePanel.add(lblMotDeviner, gbc_lblMotDeviner);

        JTextField txtIndice1 = new JTextField();
        txtIndice1.setText("Indice 1");
        GridBagConstraints gbc_txtIndice1 = new GridBagConstraints();
        gbc_txtIndice1.insets = new Insets(0, 0, 5, 5);
        gbc_txtIndice1.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtIndice1.gridx = 1;
        gbc_txtIndice1.gridy = 2;
        enigmePanel.add(txtIndice1, gbc_txtIndice1);
        txtIndice1.setColumns(10);

        JTextField txtIndice2 = new JTextField();
        txtIndice2.setText("Indice 2");
        GridBagConstraints gbc_txtIndice2 = new GridBagConstraints();
        gbc_txtIndice2.insets = new Insets(0, 0, 5, 5);
        gbc_txtIndice2.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtIndice2.gridx = 1;
        gbc_txtIndice2.gridy = 3;
        enigmePanel.add(txtIndice2, gbc_txtIndice2);
        txtIndice2.setColumns(10);

        JTextField txtIndice3 = new JTextField();
        txtIndice3.setText("Indice 3");
        GridBagConstraints gbc_txtIndice3 = new GridBagConstraints();
        gbc_txtIndice3.insets = new Insets(0, 0, 5, 5);
        gbc_txtIndice3.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtIndice3.gridx = 1;
        gbc_txtIndice3.gridy = 4;
        enigmePanel.add(txtIndice3, gbc_txtIndice3);
        txtIndice3.setColumns(10);

        JButton btnEnvoyer = new JButton("Envoyer");
        GridBagConstraints gbc_btnEnvoyer = new GridBagConstraints();
        gbc_btnEnvoyer.insets = new Insets(0, 0, 5, 5);
        gbc_btnEnvoyer.gridx = 1;
        gbc_btnEnvoyer.gridy = 5;
        enigmePanel.add(btnEnvoyer, gbc_btnEnvoyer);

        JTextPane txtpnResultats = new JTextPane();
        txtpnResultats.setText(
                "Résultats1\nRésultats2\nRésultats3\nRésultats4\nRésultats5\nRésultats6\nRésultats7\nRésultats8\nRésultats9\nRésultats10");
        GridBagConstraints gbc_txtpnResultats = new GridBagConstraints();
        gbc_txtpnResultats.gridwidth = 3;
        gbc_txtpnResultats.insets = new Insets(0, 0, 5, 5);
        gbc_txtpnResultats.fill = GridBagConstraints.BOTH;
        gbc_txtpnResultats.gridx = 0;
        gbc_txtpnResultats.gridy = 6;
        enigmePanel.add(txtpnResultats, gbc_txtpnResultats);

        JLabel lblResultat = new JLabel("Résultat");
        GridBagConstraints gbc_lblResultat = new GridBagConstraints();
        gbc_lblResultat.insets = new Insets(0, 0, 5, 5);
        gbc_lblResultat.gridx = 1;
        gbc_lblResultat.gridy = 7;
        enigmePanel.add(lblResultat, gbc_lblResultat);

        JLabel lblEssaisRestants = new JLabel("Essais Y/X");
        GridBagConstraints gbc_lblEssaisRestants = new GridBagConstraints();
        gbc_lblEssaisRestants.insets = new Insets(0, 0, 0, 5);
        gbc_lblEssaisRestants.gridx = 1;
        gbc_lblEssaisRestants.gridy = 8;
        enigmePanel.add(lblEssaisRestants, gbc_lblEssaisRestants);

        JPanel boardPanel = new JPanel();
        boardPanel.setBorder(null);
        boardPanel.setBackground(Color.WHITE);
        boardPanel.setBounds(222, 0, 578, 572);
        frmGeensonGame.getContentPane().add(boardPanel);
        boardPanel.setLayout(null);

        int boardLength = j.getPlateau().getCases().length;

        JPanel board = new JPanel();
        board.setBackground(Color.WHITE);
        board.setBounds(12, 12, 554, 548);
        boardPanel.add(board);
        board.setLayout(new GridLayout((int) Math.sqrt(boardLength), (int) Math.sqrt(boardLength), 0, 0));

        List<JPanel> cases = new ArrayList<>();

        for (int i = 0; i < boardLength; i++) {
            cases.add(new JPanel());
            cases.get(i).setLayout(new GridLayout(1, 1));

            if ((i + 1) % Math.sqrt(boardLength) == 0 && i != boardLength - 1) {
                cases.get(i).setBorder(new MatteBorder(1, 1, 0, 1, (Color) new Color(0, 0, 0)));
            }

            else if (i == boardLength - 1) {
                cases.get(i).setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
            }

            else if (i >= boardLength - Math.sqrt(boardLength)) {
                cases.get(i).setBorder(new MatteBorder(1, 1, 1, 0, (Color) new Color(0, 0, 0)));
            }

            else {
                cases.get(i).setBorder(new MatteBorder(1, 1, 0, 0, (Color) new Color(0, 0, 0)));
            }
            board.add(cases.get(i));
        }

        refreshBoard(cases, j);

        frmGeensonGame.getContentPane().revalidate();
        frmGeensonGame.getContentPane().repaint();
    }

    private void refreshBoard(List<JPanel> cases, Jeu j) {
        for (Case c : j.getPlateau().getCases()) {
            if (c instanceof CaseDepart) {
                cases.get(c.getCaseId()).setBackground(Color.yellow);
            } else if (c instanceof CaseArrivee) {
                cases.get(c.getCaseId()).setBackground(Color.green);
            } else if (c instanceof CasePrison) {
                JLabel type = new JLabel("X");
                type.setFont(new Font("Dialog", Font.PLAIN, 30));
                cases.get(c.getCaseId()).add(type);
            } else if (c instanceof CaseAvance) {
                JLabel type = new JLabel(">");
                type.setFont(new Font("Dialog", Font.PLAIN, 30));
                cases.get(c.getCaseId()).add(type);
            } else if (c instanceof CaseRecule) {
                JLabel type = new JLabel("<");
                type.setFont(new Font("Dialog", Font.PLAIN, 30));
                cases.get(c.getCaseId()).add(type);
            }
        }
    }

    private void getGameOptions() throws IOException, MethodeDeCalculException {
        frmGeensonGame.getContentPane().removeAll();
        frmGeensonGame.setLayout(null);

        JPanel optionsPanel = new JPanel();
        optionsPanel.setBounds(0, 157, 800, 411);
        frmGeensonGame.getContentPane().add(optionsPanel);
        GridBagLayout gbl_optionsPanel = new GridBagLayout();
        gbl_optionsPanel.columnWidths = new int[] { 200, 400, 200 };
        gbl_optionsPanel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        gbl_optionsPanel.columnWeights = new double[] { 0.0, 1.0, 0.0 };
        gbl_optionsPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                Double.MIN_VALUE };
        optionsPanel.setLayout(gbl_optionsPanel);

        JSplitPane joueursSplit = new JSplitPane();
        joueursSplit.setEnabled(false);
        GridBagConstraints gbc_joueursSplit = new GridBagConstraints();
        gbc_joueursSplit.insets = new Insets(0, 0, 5, 5);
        gbc_joueursSplit.gridx = 1;
        gbc_joueursSplit.gridy = 1;
        optionsPanel.add(joueursSplit, gbc_joueursSplit);

        JLabel lblNombreDeJoueurs = new JLabel("Nombre de joueurs :");
        joueursSplit.setLeftComponent(lblNombreDeJoueurs);

        JComboBox<?> nbJoueurCombo = new JComboBox<>(new Integer[] { 2, 3, 4, 5, 6 });
        joueursSplit.setRightComponent(nbJoueurCombo);

        JSplitPane boardSplit = new JSplitPane();
        boardSplit.setEnabled(false);
        GridBagConstraints gbc_boardSplit = new GridBagConstraints();
        gbc_boardSplit.insets = new Insets(0, 0, 5, 5);
        gbc_boardSplit.gridx = 1;
        gbc_boardSplit.gridy = 2;
        optionsPanel.add(boardSplit, gbc_boardSplit);

        JLabel lblTailleDuPlateau = new JLabel("Taille du plateau :");
        boardSplit.setLeftComponent(lblTailleDuPlateau);

        JComboBox<?> boardSizeCombo = new JComboBox<>(new Integer[] { 25, 36, 49 });
        boardSplit.setRightComponent(boardSizeCombo);

        JSplitPane diceSplit = new JSplitPane();
        diceSplit.setEnabled(false);
        GridBagConstraints gbc_diceSplit = new GridBagConstraints();
        gbc_diceSplit.insets = new Insets(0, 0, 5, 5);
        gbc_diceSplit.gridx = 1;
        gbc_diceSplit.gridy = 3;
        optionsPanel.add(diceSplit, gbc_diceSplit);

        JLabel lblTypeDeD = new JLabel("Type de dé :");
        diceSplit.setLeftComponent(lblTypeDeD);

        JComboBox<?> diceTypeCombo = new JComboBox<>(
                new String[] { "Dé normal (6 faces)", "Dé spécial (7 faces dont 0)" });
        diceSplit.setRightComponent(diceTypeCombo);

        JSeparator separator = new JSeparator();
        GridBagConstraints gbc_separator = new GridBagConstraints();
        gbc_separator.fill = GridBagConstraints.BOTH;
        gbc_separator.insets = new Insets(0, 0, 10, 10);
        gbc_separator.gridx = 1;
        gbc_separator.gridy = 4;
        optionsPanel.add(separator, gbc_separator);

        JSplitPane enigmeSplit = new JSplitPane();
        enigmeSplit.setEnabled(false);
        GridBagConstraints gbc_enigmeSplit = new GridBagConstraints();
        gbc_enigmeSplit.insets = new Insets(0, 0, 5, 5);
        gbc_enigmeSplit.gridx = 1;
        gbc_enigmeSplit.gridy = 5;
        optionsPanel.add(enigmeSplit, gbc_enigmeSplit);

        JLabel lblTypeDnigme = new JLabel("Type d'énigme :");
        enigmeSplit.setLeftComponent(lblTypeDnigme);

        JComboBox<?> enigmeTypeCombo = new JComboBox<>(new String[] { "Similarité cosinus", "Distance euclidienne" });
        enigmeSplit.setRightComponent(enigmeTypeCombo);

        JSplitPane triesSplit = new JSplitPane();
        triesSplit.setEnabled(false);
        GridBagConstraints gbc_triesSplit = new GridBagConstraints();
        gbc_triesSplit.insets = new Insets(0, 0, 5, 5);
        gbc_triesSplit.gridx = 1;
        gbc_triesSplit.gridy = 6;
        optionsPanel.add(triesSplit, gbc_triesSplit);

        JLabel lblNombreDessais = new JLabel("Nombre d'essais :");
        triesSplit.setLeftComponent(lblNombreDessais);

        JComboBox<?> nbTriesCombo = new JComboBox<>(new Integer[] { 1, 2, 3, 4, 5, 6 });
        triesSplit.setRightComponent(nbTriesCombo);

        JSplitPane answersSplit = new JSplitPane();
        answersSplit.setEnabled(false);
        GridBagConstraints gbc_answersSplit = new GridBagConstraints();
        gbc_answersSplit.insets = new Insets(0, 0, 5, 5);
        gbc_answersSplit.gridx = 1;
        gbc_answersSplit.gridy = 7;
        optionsPanel.add(answersSplit, gbc_answersSplit);

        JLabel lblNombreDeRponses = new JLabel("Nombre de réponses :");
        answersSplit.setLeftComponent(lblNombreDeRponses);

        JComboBox<?> nbAnswersCombo = new JComboBox<>(new Integer[] { 5, 10, 15, 20 });
        answersSplit.setRightComponent(nbAnswersCombo);

        JButton btnCommencerLaPartie = new JButton("Commencer la partie");
        btnCommencerLaPartie.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Trying to get player Names");
                int nbJoueur = (int) nbJoueurCombo.getSelectedItem();
                System.out.println(nbJoueur + " players to create");

                Joueur[] joueurs = new Joueur[nbJoueur];

                JDialog getPlayerName = new JDialog();
                getPlayerName.setBounds(100, 100, 450, 300);
                getPlayerName.getContentPane().setLayout(null);

                JPanel panel = new JPanel();
                panel.setBounds(0, 0, 450, 223);
                getPlayerName.getContentPane().add(panel);
                GridBagLayout gbl_panel = new GridBagLayout();
                gbl_panel.columnWidths = new int[] { 100, 250, 100 };
                gbl_panel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
                gbl_panel.columnWeights = new double[] { 0.0, 0.0, 0.0 };
                gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
                panel.setLayout(gbl_panel);

                JTextField[] textFields = new JTextField[nbJoueur];

                for (int i = 1; i <= nbJoueur; i++) {
                    JSplitPane splitPane = new JSplitPane();
                    splitPane.setEnabled(false);
                    GridBagConstraints gbc_splitPane = new GridBagConstraints();
                    gbc_splitPane.insets = new Insets(0, 0, 0, 5);
                    gbc_splitPane.fill = GridBagConstraints.BOTH;
                    gbc_splitPane.gridx = 1;
                    gbc_splitPane.gridy = i;
                    panel.add(splitPane, gbc_splitPane);

                    JLabel lblJoueurX = new JLabel("Joueur " + i + " :");
                    splitPane.setLeftComponent(lblJoueurX);

                    JTextField textField = new JTextField();
                    splitPane.setRightComponent(textField);
                    textField.setColumns(10);
                    textFields[i - 1] = textField;
                }

                JPanel buttonPane = new JPanel();
                buttonPane.setBounds(0, 235, 440, 35);
                buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
                getPlayerName.getContentPane().add(buttonPane);

                JButton okButton = new JButton("OK");
                buttonPane.add(okButton);
                okButton.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        getPlayerName.dispose();
                    }
                });
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getPlayerName.getRootPane().setDefaultButton(okButton);
                getPlayerName.setModal(true);
                getPlayerName.setVisible(true);

                for (int i = 0; i < nbJoueur; i++) {
                    System.out.println("Création joueur " + textFields[i].getText());
                    joueurs[i] = new Joueur(textFields[i].getText());
                }

                System.out.println("Initialisation de la partie :");

                Demere dice;
                Devinette enigme = null;

                if (diceTypeCombo.getSelectedItem().equals("Dé normal (6 faces)")) {
                    dice = new DeNormal();
                } else {
                    dice = new De0();
                }

                if (enigmeTypeCombo.getSelectedItem().equals("Similarité Cosinus")) {
                    try {
                        enigme = new Devinette(1);
                    } catch (MethodeDeCalculException e1) {
                        e1.printStackTrace();
                    }
                } else {
                    try {
                        enigme = new Devinette(2);
                    } catch (MethodeDeCalculException e1) {
                        e1.printStackTrace();
                    }
                }

                FileWriter logWriter = null;
                try {
                    logWriter = new FileWriter("log.txt");
                } catch (IOException e2) {
                    e2.printStackTrace();
                }

                Jeu j = null;

                try {
                    j = new Jeu(new Plateau((Integer) boardSizeCombo.getSelectedItem()), new VectorsMap("w2v_final3"),
                            joueurs, dice, enigme, (Integer) nbTriesCombo.getSelectedItem(),
                            (Integer) nbAnswersCombo.getSelectedItem(), true, true, logWriter);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                getGame(j);
            }
        });

        JSeparator separator2 = new JSeparator();
        GridBagConstraints gbc_separator2 = new GridBagConstraints();
        gbc_separator2.fill = GridBagConstraints.BOTH;
        gbc_separator2.insets = new Insets(0, 0, 10, 10);
        gbc_separator2.gridx = 1;
        gbc_separator2.gridy = 8;
        optionsPanel.add(separator2, gbc_separator2);

        GridBagConstraints gbc_btnCommencerLaPartie = new GridBagConstraints();
        gbc_btnCommencerLaPartie.insets = new Insets(0, 0, 5, 5);
        gbc_btnCommencerLaPartie.gridx = 1;
        gbc_btnCommencerLaPartie.gridy = 9;
        optionsPanel.add(btnCommencerLaPartie, gbc_btnCommencerLaPartie);

        frmGeensonGame.getContentPane().revalidate();
        frmGeensonGame.getContentPane().repaint();
    }

    private void getCredits() {
        frmGeensonGame.getContentPane().removeAll();

        Box verticalBox = Box.createVerticalBox();
        GridBagConstraints gbc_verticalBox = new GridBagConstraints();
        gbc_verticalBox.gridx = 1;
        gbc_verticalBox.gridy = 1;
        frmGeensonGame.getContentPane().add(verticalBox, gbc_verticalBox);

        JPanel panel = new JPanel();
        panel.setBackground(Color.WHITE);
        verticalBox.add(panel);
        // panel.setLayout(new FlowLayout());
        panel.setLayout(new GridLayout(1, 0, 10, 10));

        JLabel codeBy = new JLabel("Coded by : ");
        panel.add(codeBy);

        JTextPane sofiyaK = new JTextPane();
        sofiyaK.setText("Sofiya Kobylyanskaya\nM1 - LI");
        JTextPane rimL = new JTextPane();
        rimL.setText("Rim Loubatières\nL3 - LI");
        JTextPane nicolasM = new JTextPane();
        nicolasM.setText("Nicolas\nMerli\nL3 - LI");
        panel.add(sofiyaK);
        panel.add(rimL);
        panel.add(nicolasM);

        frmGeensonGame.getContentPane().addMouseListener(backClick);

        frmGeensonGame.getContentPane().revalidate();
        frmGeensonGame.getContentPane().repaint();
    }

    private void backMainMenu() throws IOException {
        frmGeensonGame.getContentPane().removeAll();
        frmGeensonGame.getContentPane().removeMouseListener(backClick);

        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] { 200, 200, 200 };
        gridBagLayout.rowHeights = new int[] { 150, 150, 300 };
        gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 0.0 };
        gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 1.0 };
        frmGeensonGame.getContentPane().setLayout(gridBagLayout);

        JLabel lblGeensonGame = new JLabel("Geenson Game");
        lblGeensonGame.setFont(new Font("Bitstream Charter", Font.BOLD, 30));
        GridBagConstraints gbc_lblGeensonGame = new GridBagConstraints();
        gbc_lblGeensonGame.insets = new Insets(0, 0, 5, 5);
        gbc_lblGeensonGame.gridx = 1;
        gbc_lblGeensonGame.gridy = 0;
        frmGeensonGame.getContentPane().add(lblGeensonGame, gbc_lblGeensonGame);

        JTextPane txtpnLePrincipeDu = new JTextPane();
        txtpnLePrincipeDu.setFont(new Font("FreeSans", Font.PLAIN, 16));
        txtpnLePrincipeDu.setEditable(false);
        txtpnLePrincipeDu.setText(
                "Le principe du jeu est simple, les joueurs lancent un dé à tour de rôle et avancent sur un plateau. Le but du jeu est d’arriver en premier à la case finale.\n\nÀ chaque tour, le système propose au joueur un mot m à lui faire deviner.\nLe joueur doit alors donner un certain nombre d’indices et le sytème en fonction de ceux-ci retournera les k-réponses qui lui semblent les plus pertinentes.");
        GridBagConstraints gbc_txtpnLePrincipeDu = new GridBagConstraints();
        gbc_txtpnLePrincipeDu.anchor = GridBagConstraints.BASELINE;
        gbc_txtpnLePrincipeDu.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtpnLePrincipeDu.insets = new Insets(0, 0, 5, 5);
        gbc_txtpnLePrincipeDu.gridx = 1;
        gbc_txtpnLePrincipeDu.gridy = 1;
        frmGeensonGame.getContentPane().add(txtpnLePrincipeDu, gbc_txtpnLePrincipeDu);

        Box verticalBox = Box.createVerticalBox();
        GridBagConstraints gbc_verticalBox = new GridBagConstraints();
        gbc_verticalBox.gridx = 1;
        gbc_verticalBox.gridy = 2;
        frmGeensonGame.getContentPane().add(verticalBox, gbc_verticalBox);

        JPanel panel = new JPanel();
        panel.setBackground(Color.WHITE);
        verticalBox.add(panel);
        panel.setLayout(new GridLayout(0, 1, 10, 10));

        JButton btnNewButton = new JButton("Nouvelle Partie");
        btnNewButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    getGameOptions();
                } catch (IOException | MethodeDeCalculException e1) {
                    e1.printStackTrace();
                }
                System.out.println("Trying to call options view"); // Est censé faire apparaitre la page des options
            }
        });
        panel.add(btnNewButton);

        JButton btnNewButton_1 = new JButton("Crédits");
        btnNewButton_1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                getCredits();
                System.out.println("Trying to call credits view"); // Est censé faire apparaitre la page des crédits
            }
        });
        panel.add(btnNewButton_1);

        JButton btnQuitter = new JButton("Quitter");
        btnQuitter.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }
        });
        panel.add(btnQuitter);

        frmGeensonGame.getContentPane().revalidate();
        frmGeensonGame.getContentPane().repaint();
    }

    /**
     * Initialize the contents of the Main Menu.
     */
    private void getMainMenu() {
        frmGeensonGame = new JFrame();
        frmGeensonGame.getContentPane().setFont(new Font("Dialog", Font.PLAIN, 15));
        frmGeensonGame.setTitle("Geenson Game");
        frmGeensonGame.getContentPane().setBackground(Color.WHITE);
        frmGeensonGame.setBounds(100, 100, 800, 600);
        frmGeensonGame.setResizable(false);
        frmGeensonGame.setLocationRelativeTo(null);
        frmGeensonGame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] { 200, 200, 200 };
        gridBagLayout.rowHeights = new int[] { 150, 150, 300 };
        gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 0.0 };
        gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 1.0 };
        frmGeensonGame.getContentPane().setLayout(gridBagLayout);

        JLabel lblGeensonGame = new JLabel("Geenson Game");
        lblGeensonGame.setFont(new Font("Bitstream Charter", Font.BOLD, 30));
        GridBagConstraints gbc_lblGeensonGame = new GridBagConstraints();
        gbc_lblGeensonGame.insets = new Insets(0, 0, 5, 5);
        gbc_lblGeensonGame.gridx = 1;
        gbc_lblGeensonGame.gridy = 0;
        frmGeensonGame.getContentPane().add(lblGeensonGame, gbc_lblGeensonGame);

        JTextPane txtpnLePrincipeDu = new JTextPane();
        txtpnLePrincipeDu.setFont(new Font("FreeSans", Font.PLAIN, 16));
        txtpnLePrincipeDu.setEditable(false);
        txtpnLePrincipeDu.setText(
                "Le principe du jeu est simple, les joueurs lancent un dé à tour de rôle et avancent sur un plateau. Le but du jeu est d’arriver en premier à la case finale.\n\nÀ chaque tour, le système propose au joueur un mot m à lui faire deviner.\nLe joueur doit alors donner un certain nombre d’indices et le sytème en fonction de ceux-ci retournera les k-réponses qui lui semblent les plus pertinentes.");
        GridBagConstraints gbc_txtpnLePrincipeDu = new GridBagConstraints();
        gbc_txtpnLePrincipeDu.anchor = GridBagConstraints.BASELINE;
        gbc_txtpnLePrincipeDu.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtpnLePrincipeDu.insets = new Insets(0, 0, 5, 5);
        gbc_txtpnLePrincipeDu.gridx = 1;
        gbc_txtpnLePrincipeDu.gridy = 1;
        frmGeensonGame.getContentPane().add(txtpnLePrincipeDu, gbc_txtpnLePrincipeDu);

        Box verticalBox = Box.createVerticalBox();
        GridBagConstraints gbc_verticalBox = new GridBagConstraints();
        gbc_verticalBox.gridx = 1;
        gbc_verticalBox.gridy = 2;
        frmGeensonGame.getContentPane().add(verticalBox, gbc_verticalBox);

        JPanel panel = new JPanel();
        panel.setBackground(Color.WHITE);
        verticalBox.add(panel);
        panel.setLayout(new GridLayout(0, 1, 10, 10));

        JButton btnNewButton = new JButton("Nouvelle Partie");
        btnNewButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    getGameOptions();
                } catch (IOException | MethodeDeCalculException e1) {
                    e1.printStackTrace();
                }
                System.out.println("Trying to call game view"); // Est censé faire apparaitre la page des crédits
            }
        });
        panel.add(btnNewButton);

        JButton btnNewButton_1 = new JButton("Crédits");
        btnNewButton_1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                getCredits();
                System.out.println("Trying to call credits view"); // Est censé faire apparaitre la page des crédits
            }
        });
        panel.add(btnNewButton_1);

        JButton btnQuitter = new JButton("Quitter");
        btnQuitter.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }
        });
        panel.add(btnQuitter);
    }
}