package jeu.gui.affichage;

import javax.swing.JFrame;

public class Frame extends JFrame {

    /** Configuration basique pour une nouvelle fenêtre JFrame */

    protected int height;
    protected int width;

    public Frame(String title, int h, int w) {
        setTitle(title);
        setSize(w, h);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);

        this.height = h;
        this.width = w;
        this.add(new Panel(this));
    }
}