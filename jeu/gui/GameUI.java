package jeu.gui;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;

public class GameUI {

    private JFrame mainFrame;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    GameUI window = new GameUI();
                    window.mainFrame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public GameUI() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        mainFrame = new JFrame();
        mainFrame.setBounds(100, 100, 800, 600);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setTitle("Geenson Game");
        mainFrame.setResizable(false);
        mainFrame.setLocationRelativeTo(null);

        MainMenu mainMenu = new MainMenu(mainFrame);
        mainFrame.getContentPane().add(mainMenu);

    }

}
