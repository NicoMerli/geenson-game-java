package jeu.gui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;

public class Credits extends JPanel {

    public Credits(JFrame mainFrame) {

        Box verticalBox = Box.createVerticalBox();
        GridBagConstraints gbc_verticalBox = new GridBagConstraints();
        gbc_verticalBox.gridx = 1;
        gbc_verticalBox.gridy = 1;
        this.add(verticalBox, gbc_verticalBox);

        JPanel panel = new JPanel();
        panel.setBackground(Color.WHITE);
        verticalBox.add(panel);
        panel.setLayout(new GridLayout(1, 0, 10, 10));

        JLabel codeBy = new JLabel("Coded by : ");
        panel.add(codeBy);

        JTextPane sofiyaK = new JTextPane();
        sofiyaK.setText("Sofiya Kobylyanskaya\nM1 - LI");
        JTextPane rimL = new JTextPane();
        rimL.setText("Rim Loubatières\nL3 - LI");
        JTextPane nicolasM = new JTextPane();
        nicolasM.setText("Nicolas\nMerli\nL3 - LI");
        panel.add(sofiyaK);
        panel.add(rimL);
        panel.add(nicolasM);

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                mainFrame.getContentPane().removeAll();

                MainMenu mainMenu = new MainMenu(mainFrame);
                mainFrame.getContentPane().add(mainMenu);

                mainFrame.getContentPane().revalidate();
                mainFrame.getContentPane().repaint();
            }
        });

    }
}