package jeu.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;

import jeu.Jeu;
import jeu.Joueur;
import jeu.plateau.cases.Case;
import jeu.plateau.cases.CaseArrivee;
import jeu.plateau.cases.CaseAvance;
import jeu.plateau.cases.CaseDepart;
import jeu.plateau.cases.CasePrison;
import jeu.plateau.cases.CaseRecule;

public class GameView extends JPanel {

    private Jeu jeu;
    private List<JPanel> casesPanels;
    private JButton btnLancerLeD;
    private JLabel lblTourDeJoueur;
    private JLabel lblDiceRoll;

    public GameView(Jeu j, JFrame mainFrame) {
        this.jeu = j;

        mainFrame.setBounds(100, 100, 1000, 600);
        mainFrame.setLayout(null);

        JPanel logPanel = new JPanel();
        logPanel.setBounds(800, 0, 200, 568);
        mainFrame.getContentPane().add(logPanel);

        JTextArea txtPanelLog = new JTextArea();
        txtPanelLog.setEditable(false);

        JScrollPane scrollPane = new JScrollPane(txtPanelLog);
        scrollPane.setPreferredSize(new Dimension(200, 550));
        logPanel.add(scrollPane);

        String log = "";
        log += "Début de la partie\n";
        txtPanelLog.setText(log);
        log += j.toString();
        txtPanelLog.setText(log);

        JPanel sidePanel = new JPanel();
        sidePanel.setBorder(new LineBorder(new Color(0, 0, 0)));
        sidePanel.setBounds(0, 0, 223, 572);
        mainFrame.getContentPane().add(sidePanel);
        sidePanel.setLayout(null);

        JPanel dicePanel = new JPanel();
        dicePanel.setBackground(Color.DARK_GRAY);
        dicePanel.setBounds(12, 429, 200, 134);
        sidePanel.add(dicePanel);
        GridBagLayout gbl_dicePanel = new GridBagLayout();
        gbl_dicePanel.rowHeights = new int[] { 10, 16, 60 };
        gbl_dicePanel.columnWidths = new int[] { 50, 100, 50 };
        gbl_dicePanel.columnWeights = new double[] { 0.0, 0.0, 0.0 };
        gbl_dicePanel.rowWeights = new double[] { 0.0, 0.0, 0.0 };
        dicePanel.setLayout(gbl_dicePanel);

        // Random rand = new Random();

        btnLancerLeD = new JButton("Lancer le dé");
        GridBagConstraints gbc_btnLancerLeD = new GridBagConstraints();
        gbc_btnLancerLeD.insets = new Insets(0, 0, 5, 5);
        gbc_btnLancerLeD.gridx = 1;
        gbc_btnLancerLeD.gridy = 0;
        btnLancerLeD.setEnabled(false);
        dicePanel.add(btnLancerLeD, gbc_btnLancerLeD);

        lblDiceRoll = new JLabel("");
        lblDiceRoll.setForeground(Color.WHITE);
        lblDiceRoll.setBackground(new Color(238, 238, 238));
        lblDiceRoll.setFont(new Font("Dialog", Font.BOLD, 60));
        GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
        gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
        gbc_lblNewLabel.gridx = 1;
        gbc_lblNewLabel.gridy = 2;
        dicePanel.add(lblDiceRoll, gbc_lblNewLabel);

        JPanel playerPanel = new JPanel();
        playerPanel.setBounds(12, 12, 199, 30);
        sidePanel.add(playerPanel);
        playerPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        lblTourDeJoueur = new JLabel("Tour de playerName");
        playerPanel.add(lblTourDeJoueur);

        JPanel enigmePanel = new JPanel();
        enigmePanel.setBackground(Color.GRAY);
        enigmePanel.setBounds(0, 54, 223, 363);
        sidePanel.add(enigmePanel);
        GridBagLayout gbl_enigmePanel = new GridBagLayout();
        gbl_enigmePanel.columnWidths = new int[] { 50, 100, 50 };
        gbl_enigmePanel.rowHeights = new int[] { 20, 0, 0, 0, 0, 0, 30, 0, 0, 30 };
        gbl_enigmePanel.columnWeights = new double[] { 0.0, 1.0, 0.0 };
        gbl_enigmePanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
        enigmePanel.setLayout(gbl_enigmePanel);

        JLabel lblMotDeviner = new JLabel("Mot à deviner");
        GridBagConstraints gbc_lblMotDeviner = new GridBagConstraints();
        gbc_lblMotDeviner.insets = new Insets(0, 0, 5, 5);
        gbc_lblMotDeviner.gridx = 1;
        gbc_lblMotDeviner.gridy = 1;
        enigmePanel.add(lblMotDeviner, gbc_lblMotDeviner);

        JTextField txtIndice1 = new JTextField();
        txtIndice1.setText("Indice 1");
        GridBagConstraints gbc_txtIndice1 = new GridBagConstraints();
        gbc_txtIndice1.insets = new Insets(0, 0, 5, 5);
        gbc_txtIndice1.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtIndice1.gridx = 1;
        gbc_txtIndice1.gridy = 2;
        enigmePanel.add(txtIndice1, gbc_txtIndice1);
        txtIndice1.setColumns(10);

        JTextField txtIndice2 = new JTextField();
        txtIndice2.setText("Indice 2");
        GridBagConstraints gbc_txtIndice2 = new GridBagConstraints();
        gbc_txtIndice2.insets = new Insets(0, 0, 5, 5);
        gbc_txtIndice2.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtIndice2.gridx = 1;
        gbc_txtIndice2.gridy = 3;
        enigmePanel.add(txtIndice2, gbc_txtIndice2);
        txtIndice2.setColumns(10);

        JTextField txtIndice3 = new JTextField();
        txtIndice3.setText("Indice 3");
        GridBagConstraints gbc_txtIndice3 = new GridBagConstraints();
        gbc_txtIndice3.insets = new Insets(0, 0, 5, 5);
        gbc_txtIndice3.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtIndice3.gridx = 1;
        gbc_txtIndice3.gridy = 4;
        enigmePanel.add(txtIndice3, gbc_txtIndice3);
        txtIndice3.setColumns(10);

        JButton btnEnvoyer = new JButton("Envoyer");
        GridBagConstraints gbc_btnEnvoyer = new GridBagConstraints();
        gbc_btnEnvoyer.insets = new Insets(0, 0, 5, 5);
        gbc_btnEnvoyer.gridx = 1;
        gbc_btnEnvoyer.gridy = 5;
        enigmePanel.add(btnEnvoyer, gbc_btnEnvoyer);

        JTextPane txtpnResultats = new JTextPane();
        txtpnResultats.setText(
                "Résultats1\nRésultats2\nRésultats3\nRésultats4\nRésultats5\nRésultats6\nRésultats7\nRésultats8\nRésultats9\nRésultats10");
        GridBagConstraints gbc_txtpnResultats = new GridBagConstraints();
        gbc_txtpnResultats.gridwidth = 3;
        gbc_txtpnResultats.insets = new Insets(0, 0, 5, 5);
        gbc_txtpnResultats.fill = GridBagConstraints.BOTH;
        gbc_txtpnResultats.gridx = 0;
        gbc_txtpnResultats.gridy = 6;
        enigmePanel.add(txtpnResultats, gbc_txtpnResultats);

        JLabel lblResultat = new JLabel("Résultat");
        GridBagConstraints gbc_lblResultat = new GridBagConstraints();
        gbc_lblResultat.insets = new Insets(0, 0, 5, 5);
        gbc_lblResultat.gridx = 1;
        gbc_lblResultat.gridy = 7;
        enigmePanel.add(lblResultat, gbc_lblResultat);

        JLabel lblEssaisRestants = new JLabel("Essais Y/X");
        GridBagConstraints gbc_lblEssaisRestants = new GridBagConstraints();
        gbc_lblEssaisRestants.insets = new Insets(0, 0, 0, 5);
        gbc_lblEssaisRestants.gridx = 1;
        gbc_lblEssaisRestants.gridy = 8;
        enigmePanel.add(lblEssaisRestants, gbc_lblEssaisRestants);

        JPanel boardPanel = new JPanel();
        boardPanel.setBorder(null);
        boardPanel.setBackground(Color.WHITE);
        boardPanel.setBounds(222, 0, 578, 572);
        mainFrame.getContentPane().add(boardPanel);
        boardPanel.setLayout(null);

        int boardLength = j.getPlateau().getCases().length;

        JPanel board = new JPanel();
        board.setBackground(Color.WHITE);
        board.setBounds(12, 12, 554, 548);
        boardPanel.add(board);
        board.setLayout(new GridLayout((int) Math.sqrt(boardLength), (int) Math.sqrt(boardLength), 0, 0));

        casesPanels = new ArrayList<>();

        for (int i = 0; i < boardLength; i++) {
            casesPanels.add(new JPanel());
            casesPanels.get(i).setLayout(new GridLayout(3, 3));

            if ((i + 1) % Math.sqrt(boardLength) == 0 && i != boardLength - 1) {
                casesPanels.get(i).setBorder(new MatteBorder(1, 1, 0, 1, (Color) new Color(0, 0, 0)));
            }

            else if (i == boardLength - 1) {
                casesPanels.get(i).setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
            }

            else if (i >= boardLength - Math.sqrt(boardLength)) {
                casesPanels.get(i).setBorder(new MatteBorder(1, 1, 1, 0, (Color) new Color(0, 0, 0)));
            }

            else {
                casesPanels.get(i).setBorder(new MatteBorder(1, 1, 0, 0, (Color) new Color(0, 0, 0)));
            }
            board.add(casesPanels.get(i));
        }

        initBoard();
        jouer();
    }

    private void initBoard() {
        for (Case c : jeu.getPlateau().getCases()) {
            if (c instanceof CaseDepart) {
                casesPanels.get(c.getCaseId()).setBackground(Color.yellow);
            } else if (c instanceof CaseArrivee) {
                casesPanels.get(c.getCaseId()).setBackground(Color.green);
            } else if (c instanceof CasePrison) {
                JLabel type = new JLabel("X");
                type.setFont(new Font("Dialog", Font.PLAIN, 30));
                casesPanels.get(c.getCaseId()).add(type);
            } else if (c instanceof CaseAvance) {
                JLabel type = new JLabel(">");
                type.setFont(new Font("Dialog", Font.PLAIN, 30));
                casesPanels.get(c.getCaseId()).add(type);
            } else if (c instanceof CaseRecule) {
                JLabel type = new JLabel("<");
                type.setFont(new Font("Dialog", Font.PLAIN, 30));
                casesPanels.get(c.getCaseId()).add(type);
            }
        }
    }

    private void refreshBoard() {

        for (Joueur joueur : jeu.getJoueurs()) {
            JLabel playerName = new JLabel(joueur.getName());
            casesPanels.get(joueur.getCase().getCaseId()).add(playerName);
        }

    }

    private void jouer() {

        // On place les joueurs au début du plateau
        for (Joueur joueur : jeu.getJoueurs()) {
            joueur.setCase(jeu.getPlateau().getCases()[0]);
        }
        jeu.getPlateau().getCases()[0].setJoueurs(jeu.getJoueurs());
        refreshBoard();
    }
}