package jeu.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;

public class MainMenu extends JPanel {

    public MainMenu(JFrame mainFrame) {

        this.setBackground(Color.WHITE);

        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] { 200, 200, 200 };
        gridBagLayout.rowHeights = new int[] { 150, 150, 300 };
        gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 0.0 };
        gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 1.0 };
        this.setLayout(gridBagLayout);

        JLabel lblGeensonGame = new JLabel("Geenson Game");
        lblGeensonGame.setFont(new Font("Bitstream Charter", Font.BOLD, 30));
        GridBagConstraints gbc_lblGeensonGame = new GridBagConstraints();
        gbc_lblGeensonGame.insets = new Insets(0, 0, 5, 5);
        gbc_lblGeensonGame.gridx = 1;
        gbc_lblGeensonGame.gridy = 0;
        this.add(lblGeensonGame, gbc_lblGeensonGame);

        JTextPane txtpnLePrincipeDu = new JTextPane();
        txtpnLePrincipeDu.setFont(new Font("FreeSans", Font.PLAIN, 16));
        txtpnLePrincipeDu.setEditable(false);
        txtpnLePrincipeDu.setText(
                "Le principe du jeu est simple, les joueurs lancent un dé à tour de rôle et avancent sur un plateau. Le but du jeu est d’arriver en premier à la case finale.\n\nÀ chaque tour, le système propose au joueur un mot m à lui faire deviner.\nLe joueur doit alors donner un certain nombre d’indices et le sytème en fonction de ceux-ci retournera les k-réponses qui lui semblent les plus pertinentes.");
        GridBagConstraints gbc_txtpnLePrincipeDu = new GridBagConstraints();
        gbc_txtpnLePrincipeDu.anchor = GridBagConstraints.BASELINE;
        gbc_txtpnLePrincipeDu.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtpnLePrincipeDu.insets = new Insets(0, 0, 5, 5);
        gbc_txtpnLePrincipeDu.gridx = 1;
        gbc_txtpnLePrincipeDu.gridy = 1;
        this.add(txtpnLePrincipeDu, gbc_txtpnLePrincipeDu);

        Box verticalBox = Box.createVerticalBox();
        GridBagConstraints gbc_verticalBox = new GridBagConstraints();
        gbc_verticalBox.gridx = 1;
        gbc_verticalBox.gridy = 2;
        this.add(verticalBox, gbc_verticalBox);

        JPanel panel = new JPanel();
        panel.setBackground(Color.WHITE);
        verticalBox.add(panel);
        panel.setLayout(new GridLayout(0, 1, 10, 10));

        JButton btnNewButton = new JButton("Nouvelle Partie");
        btnNewButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Trying to call game view"); // Est censé faire apparaitre la page des crédits
                mainFrame.getContentPane().removeAll();

                Options optionsView = new Options(mainFrame);
                mainFrame.add(optionsView);

                mainFrame.getContentPane().revalidate();
                mainFrame.getContentPane().repaint();
            }
        });
        panel.add(btnNewButton);

        JButton btnNewButton_1 = new JButton("Crédits");
        btnNewButton_1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Trying to call credits view"); // Est censé faire apparaitre la page des crédits
                mainFrame.getContentPane().removeAll();

                Credits creditsPanel = new Credits(mainFrame);
                mainFrame.add(creditsPanel);

                mainFrame.getContentPane().revalidate();
                mainFrame.getContentPane().repaint();

            }
        });
        panel.add(btnNewButton_1);

        JButton btnQuitter = new JButton("Quitter");
        btnQuitter.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }
        });
        panel.add(btnQuitter);
    }
}