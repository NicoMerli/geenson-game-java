package jeu.gui;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;

import jeu.Joueur;

public class GetPlayerNamesDialog extends JDialog {

    protected JTextField[] textFields;
    protected Joueur[] joueurs;
    protected int nbJoueurs;

    public GetPlayerNamesDialog(int nbJoueurs, Joueur[] joueurs) {
        this.nbJoueurs = nbJoueurs;
        this.joueurs = joueurs;

        this.setBounds(100, 100, 450, 300);
        this.getContentPane().setLayout(null);

        JPanel panel = new JPanel();
        panel.setBounds(0, 0, 450, 223);
        this.getContentPane().add(panel);
        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[] { 100, 250, 100 };
        gbl_panel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
        gbl_panel.columnWeights = new double[] { 0.0, 0.0, 0.0 };
        gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
        panel.setLayout(gbl_panel);

        textFields = new JTextField[nbJoueurs];

        for (int i = 1; i <= nbJoueurs; i++) {
            JSplitPane splitPane = new JSplitPane();
            splitPane.setEnabled(false);
            GridBagConstraints gbc_splitPane = new GridBagConstraints();
            gbc_splitPane.insets = new Insets(0, 0, 0, 5);
            gbc_splitPane.fill = GridBagConstraints.BOTH;
            gbc_splitPane.gridx = 1;
            gbc_splitPane.gridy = i;
            panel.add(splitPane, gbc_splitPane);

            JLabel lblJoueurX = new JLabel("Joueur " + i + " :");
            splitPane.setLeftComponent(lblJoueurX);

            JTextField textField = new JTextField();
            splitPane.setRightComponent(textField);
            textField.setColumns(10);
            textFields[i - 1] = textField;
        }

        JPanel buttonPane = new JPanel();
        buttonPane.setBounds(0, 235, 440, 35);
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        this.getContentPane().add(buttonPane);

        JButton okButton = new JButton("OK");
        buttonPane.add(okButton);
        okButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
            }
        });
        okButton.setActionCommand("OK");
        buttonPane.add(okButton);
        this.getRootPane().setDefaultButton(okButton);
        this.setModal(true);
        this.setVisible(true);

        createJoueurs();

    }

    public void createJoueurs() {
        for (int i = 0; i < nbJoueurs; i++) {
            joueurs[i] = new Joueur(textFields[i].getText());
        }
    }

}