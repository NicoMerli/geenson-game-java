package jeu.gui;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTextField;

import jeu.Jeu;
import jeu.Joueur;
import jeu.des.De0;
import jeu.des.DeNormal;
import jeu.des.Demere;
import jeu.devinette.Devinette;
import jeu.exceptions.MethodeDeCalculException;
import jeu.plateau.Plateau;
import jeu.vecteurs.VectorsMap;

public class Options extends JPanel {

    private JComboBox<?> nbJoueursCombo;
    private JComboBox<?> boardSizeCombo;
    private JComboBox<?> diceTypeCombo;
    private JComboBox<?> enigmeTypeCombo;
    private JComboBox<?> nbTriesCombo;
    private JComboBox<?> nbAnswersCombo;

    public Options(JFrame mainFrame) {
        Options options = this;

        mainFrame.setLayout(null);

        JPanel optionsPanel = new JPanel();
        optionsPanel.setBounds(0, 157, 800, 411);
        mainFrame.getContentPane().add(optionsPanel);

        GridBagLayout gbl_optionsPanel = new GridBagLayout();
        gbl_optionsPanel.columnWidths = new int[] { 200, 400, 200 };
        gbl_optionsPanel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        gbl_optionsPanel.columnWeights = new double[] { 0.0, 1.0, 0.0 };
        gbl_optionsPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                Double.MIN_VALUE };
        optionsPanel.setLayout(gbl_optionsPanel);

        JSplitPane joueursSplit = new JSplitPane();
        joueursSplit.setEnabled(false);
        GridBagConstraints gbc_joueursSplit = new GridBagConstraints();
        gbc_joueursSplit.insets = new Insets(0, 0, 5, 5);
        gbc_joueursSplit.gridx = 1;
        gbc_joueursSplit.gridy = 1;
        optionsPanel.add(joueursSplit, gbc_joueursSplit);

        JLabel lblNombreDeJoueurs = new JLabel("Nombre de joueurs :");
        joueursSplit.setLeftComponent(lblNombreDeJoueurs);

        nbJoueursCombo = new JComboBox<>(new Integer[] { 2, 3, 4, 5, 6 });
        joueursSplit.setRightComponent(nbJoueursCombo);

        JSplitPane boardSplit = new JSplitPane();
        boardSplit.setEnabled(false);
        GridBagConstraints gbc_boardSplit = new GridBagConstraints();
        gbc_boardSplit.insets = new Insets(0, 0, 5, 5);
        gbc_boardSplit.gridx = 1;
        gbc_boardSplit.gridy = 2;
        optionsPanel.add(boardSplit, gbc_boardSplit);

        JLabel lblTailleDuPlateau = new JLabel("Taille du plateau :");
        boardSplit.setLeftComponent(lblTailleDuPlateau);

        boardSizeCombo = new JComboBox<>(new Integer[] { 25, 36, 49 });
        boardSplit.setRightComponent(boardSizeCombo);

        JSplitPane diceSplit = new JSplitPane();
        diceSplit.setEnabled(false);
        GridBagConstraints gbc_diceSplit = new GridBagConstraints();
        gbc_diceSplit.insets = new Insets(0, 0, 5, 5);
        gbc_diceSplit.gridx = 1;
        gbc_diceSplit.gridy = 3;
        optionsPanel.add(diceSplit, gbc_diceSplit);

        JLabel lblTypeDeD = new JLabel("Type de dé :");
        diceSplit.setLeftComponent(lblTypeDeD);

        diceTypeCombo = new JComboBox<>(new String[] { "Dé normal (6 faces)", "Dé spécial (7 faces dont 0)" });
        diceSplit.setRightComponent(diceTypeCombo);

        JSeparator separator = new JSeparator();
        GridBagConstraints gbc_separator = new GridBagConstraints();
        gbc_separator.fill = GridBagConstraints.BOTH;
        gbc_separator.insets = new Insets(0, 0, 10, 10);
        gbc_separator.gridx = 1;
        gbc_separator.gridy = 4;
        optionsPanel.add(separator, gbc_separator);

        JSplitPane enigmeSplit = new JSplitPane();
        enigmeSplit.setEnabled(false);
        GridBagConstraints gbc_enigmeSplit = new GridBagConstraints();
        gbc_enigmeSplit.insets = new Insets(0, 0, 5, 5);
        gbc_enigmeSplit.gridx = 1;
        gbc_enigmeSplit.gridy = 5;
        optionsPanel.add(enigmeSplit, gbc_enigmeSplit);

        JLabel lblTypeDnigme = new JLabel("Type d'énigme :");
        enigmeSplit.setLeftComponent(lblTypeDnigme);

        enigmeTypeCombo = new JComboBox<>(new String[] { "Similarité cosinus", "Distance euclidienne" });
        enigmeSplit.setRightComponent(enigmeTypeCombo);

        JSplitPane triesSplit = new JSplitPane();
        triesSplit.setEnabled(false);
        GridBagConstraints gbc_triesSplit = new GridBagConstraints();
        gbc_triesSplit.insets = new Insets(0, 0, 5, 5);
        gbc_triesSplit.gridx = 1;
        gbc_triesSplit.gridy = 6;
        optionsPanel.add(triesSplit, gbc_triesSplit);

        JLabel lblNombreDessais = new JLabel("Nombre d'essais :");
        triesSplit.setLeftComponent(lblNombreDessais);

        nbTriesCombo = new JComboBox<>(new Integer[] { 1, 2, 3, 4, 5, 6 });
        triesSplit.setRightComponent(nbTriesCombo);

        JSplitPane answersSplit = new JSplitPane();
        answersSplit.setEnabled(false);
        GridBagConstraints gbc_answersSplit = new GridBagConstraints();
        gbc_answersSplit.insets = new Insets(0, 0, 5, 5);
        gbc_answersSplit.gridx = 1;
        gbc_answersSplit.gridy = 7;
        optionsPanel.add(answersSplit, gbc_answersSplit);

        JLabel lblNombreDeRponses = new JLabel("Nombre de réponses :");
        answersSplit.setLeftComponent(lblNombreDeRponses);

        nbAnswersCombo = new JComboBox<>(new Integer[] { 5, 10, 15, 20 });
        answersSplit.setRightComponent(nbAnswersCombo);

        JButton btnCommencerLaPartie = new JButton("Commencer la partie");
        btnCommencerLaPartie.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                createGame(mainFrame);
            }
        });

        JSeparator separator2 = new JSeparator();
        GridBagConstraints gbc_separator2 = new GridBagConstraints();
        gbc_separator2.fill = GridBagConstraints.BOTH;
        gbc_separator2.insets = new Insets(0, 0, 10, 10);
        gbc_separator2.gridx = 1;
        gbc_separator2.gridy = 8;
        optionsPanel.add(separator2, gbc_separator2);

        GridBagConstraints gbc_btnCommencerLaPartie = new GridBagConstraints();
        gbc_btnCommencerLaPartie.insets = new Insets(0, 0, 5, 5);
        gbc_btnCommencerLaPartie.gridx = 1;
        gbc_btnCommencerLaPartie.gridy = 9;
        optionsPanel.add(btnCommencerLaPartie, gbc_btnCommencerLaPartie);
    }

    public void createGame(JFrame mainFrame) {
        int nbJoueurs = (int) nbJoueursCombo.getSelectedItem();

        Joueur[] joueurs = new Joueur[nbJoueurs];

        JDialog getPlayerName = new JDialog(mainFrame);
        getPlayerName.setBounds(100, 100, 450, 300);
        getPlayerName.getContentPane().setLayout(null);

        JPanel panel = new JPanel();
        panel.setBounds(0, 0, 450, 223);
        getPlayerName.getContentPane().add(panel);
        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[] { 100, 250, 100 };
        gbl_panel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
        gbl_panel.columnWeights = new double[] { 0.0, 0.0, 0.0 };
        gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
        panel.setLayout(gbl_panel);

        JTextField[] textFields = new JTextField[nbJoueurs];

        for (int i = 1; i <= nbJoueurs; i++) {
            JSplitPane splitPane = new JSplitPane();
            splitPane.setEnabled(false);
            GridBagConstraints gbc_splitPane = new GridBagConstraints();
            gbc_splitPane.insets = new Insets(0, 0, 0, 5);
            gbc_splitPane.fill = GridBagConstraints.BOTH;
            gbc_splitPane.gridx = 1;
            gbc_splitPane.gridy = i;
            panel.add(splitPane, gbc_splitPane);

            JLabel lblJoueurX = new JLabel("Joueur " + i + " :");
            splitPane.setLeftComponent(lblJoueurX);

            JTextField textField = new JTextField();
            splitPane.setRightComponent(textField);
            textField.setColumns(10);
            textFields[i - 1] = textField;
        }

        JPanel buttonPane = new JPanel();
        buttonPane.setBounds(0, 235, 440, 35);
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getPlayerName.getContentPane().add(buttonPane);

        JButton okButton = new JButton("OK");
        buttonPane.add(okButton);
        okButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                getPlayerName.dispose();
            }
        });
        okButton.setActionCommand("OK");
        buttonPane.add(okButton);
        getPlayerName.getRootPane().setDefaultButton(okButton);
        getPlayerName.setModal(true);
        getPlayerName.setVisible(true);

        for (int i = 0; i < nbJoueurs; i++) {
            joueurs[i] = new Joueur(textFields[i].getText());
        }

        Demere dice;
        Devinette enigme = null;

        if (diceTypeCombo.getSelectedItem().equals("Dé normal (6 faces)")) {
            dice = new DeNormal();
        } else {
            dice = new De0();
        }

        if (enigmeTypeCombo.getSelectedItem().equals("Similarité Cosinus")) {
            try {
                enigme = new Devinette(1);
            } catch (MethodeDeCalculException e1) {
                e1.printStackTrace();
            }
        } else {
            try {
                enigme = new Devinette(2);
            } catch (MethodeDeCalculException e1) {
                e1.printStackTrace();
            }
        }

        FileWriter logWriter = null;
        try {
            logWriter = new FileWriter("log.txt");
        } catch (IOException e2) {
            e2.printStackTrace();
        }

        Jeu j = null;

        try {
            j = new Jeu(new Plateau((Integer) boardSizeCombo.getSelectedItem()), new VectorsMap("w2v_final3"), joueurs,
                    dice, enigme, (Integer) nbTriesCombo.getSelectedItem(), (Integer) nbAnswersCombo.getSelectedItem(),
                    true, true, logWriter);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        mainFrame.getContentPane().removeAll();
        GameView gameView = new GameView(j, mainFrame);
        mainFrame.add(gameView);

        mainFrame.getContentPane().revalidate();
        mainFrame.getContentPane().repaint();
    }
}