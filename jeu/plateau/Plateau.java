package jeu.plateau;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;

import jeu.plateau.cases.Case;
import jeu.plateau.cases.CaseArrivee;
import jeu.plateau.cases.CaseAvance;
import jeu.plateau.cases.CaseDepart;
import jeu.plateau.cases.CaseNormale;
import jeu.plateau.cases.CasePrison;
import jeu.plateau.cases.CaseRecule;

    /**
     *Classe représentant le plateau du jeu
     */
public class Plateau implements Iterable<Case> {

    private Case[] cases;

    /**
     *Constructeur de la classe
     *@param size le nombre des cases du plateau
     */
    
    public Plateau(int size) {
        cases = new Case[size];
        cases[0] = new CaseDepart();
        for (int i = 1; i < size - 1; i++) {
            Random rand = new Random();
            int r = rand.nextInt(8);
            if (r == 0) {
                cases[i] = new CasePrison();
            } else if (r == 1 && i < size - 2) {
                cases[i] = new CaseAvance();
            } else if (r == 2 && i > 3) {
                cases[i] = new CaseRecule();
            } else {
                cases[i] = new CaseNormale();
            }
        }
        cases[size - 1] = new CaseArrivee();
    }

        /**
         *Méthode renvoyant la représentation du plateau permettant de l'imprimer dans le terminal
         *@return String représentant le plateau
         */
    
    public String printPlateauCLI() {
        String s = "";
        for (int i = 0; i < this.cases.length; i++) {
            if ((i + 1) % Math.sqrt(this.cases.length) == 0)
                s += this.cases[i].toString() + " \n";
            else
                s += this.cases[i].toString() + " ";
        }
        return s;
    }

        /**
         *Méthode permettant d'imprimer les cases du plateau dans le terminal
         *@param c la case
         *@param nbJ le nombre de joueurs sur la case
         *@see Case
         *@see Case#printJoueurs(int)
         *@see Case#getCaseId()
         *@see Case#printTypeCLI()
         */
    
    public static void printCase(Case c, int nbJ) {
        if (c.getCaseId() < 9) {
            System.out.print("|" + c.printTypeCLI() + c.printJoueurs(nbJ) + "   " + (c.getCaseId() + 1));
        } else {
            System.out.print("|" + c.printTypeCLI() + " " + c.printJoueurs(nbJ) + " " + (c.getCaseId() + 1));
        }

    }

        /**
         *Méthode permettant d'imprimer le plateau dans le terminal
         *@param nbJ le nombre de joueurs
         *@see Plateau#printCase(Case, int)
         *@see Plateau#getCases()
         */
    
    public void printPlateau(int nbJ) {
        // System.out.println("Map du plateau :\n\n" + this.printPlateauCLI());
        for (Case c : this.getCases()) {
            if ((c.getCaseId() + 1) % Math.sqrt(this.getCases().length) == 0) {
                printCase(c, nbJ);
                System.out.println("|");
            } else {
                printCase(c, nbJ);
            }
        }
    }

        /**
         *Constructeur qui crée un nouveau plateau en prenant en paramètre un tableau de cases
         *@param c un tableau de cases
         */
    
    public Plateau(Case[] c) {
        cases = c;
    }

        /**
         *Méthode qui renvoie les cases du plateau
         *@return un tableau des cases du plateau
         */
         
    public Case[] getCases() {
        return this.cases;
    }

        /**
         *Méthode qui renvoie la représentation des cases du plateau
         *@return la représentation des cases du plateau (String)
         *@see Case#toString()
         */
         
    public String toString() {
        String s = "";
        for (Case c : this) {
            s += c.toString() + "\n";
        }
        return s;
    }

    @Override
    public Iterator<Case> iterator() {
        return (new ArrayList<Case>(Arrays.asList(cases))).iterator();
    }
}
