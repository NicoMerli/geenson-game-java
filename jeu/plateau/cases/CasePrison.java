package jeu.plateau.cases;

import jeu.Joueur;
import jeu.plateau.Plateau;

/**
 * Classe représentant la case Prison qui ne permet pas au joueur d'avancer sur
 * le plateau
 * @inheritDoc Case
 * @see Case
 */

public class CasePrison extends Case {
	/**
	 * Méthode qui renvoie la représentation textuelle de la case
	 * 
	 * @return la représentation textuelle de la case
	 */
	@Override
	public String toString() {
		return "Prison";
	}

	/**
	 * Méthode qui renvoie un booléen indiquant si c'est la dernière case du plateau
	 * 
	 * @return false car la case de type Prison ne peut pas être finale
	 */
	@Override
	public boolean isFinale() {
		return false;
	}

	/**
	 * Méthode qui ne permet pas au joueur d'avancer sur le plateau et met son
	 * champs d'instance isInPrison à true
	 * 
	 * @param p le plateau
	 * @param j le joueur
	 * @see Joueur#isInPrison
	 * @see Joueur#peutRejouer
	 */
	@Override
	public void effect(Plateau p, Joueur j) { // le joueur reste sur la même position
		j.setIsInPrison(true);
		j.setPeutRejouer(false);
	}

	/**
	 * Méthode qui renvoie la représentation de la case pour l'impression du plateau
	 * 
	 * @return la représentation du type de la case
	 */
	@Override
	public String printTypeCLI() {
		return "X";
	}
}
