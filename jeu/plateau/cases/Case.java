package jeu.plateau.cases;

import jeu.Joueur;
import jeu.plateau.Plateau;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Classe abstraite représentant une case du plateau
 */

public abstract class Case {

    // protected Joueur[] joueurs;
    private ArrayList<Joueur> joueurs;
    private static int id;
    private int CaseId;

    /**
     * Constructeur de la classe qui crée une nouvelle case
     */

    public Case() {
        // this.joueurs = null;
        this.joueurs = new ArrayList<Joueur>();
        this.CaseId = id;
        id++;
    }

    /**
     * Méthode qui renvoie le numéro de la case
     * 
     * @return la numéro de la case
     */

    public int getCaseId() {
        return this.CaseId;
    }

    /**
     * Méthode qui attibue les joueurs mis en paramètre à la case
     * 
     * @param j représentant une ArrayList de joueurs
     * @see Joueur
     */
    public void setJoueurs(ArrayList<Joueur> j) {
        this.joueurs = j;
    }

    /**
     * Méthode qui met à jour les joueurs de la case
     * 
     * @param joueurs un tableau de joueurs
     * @see Joueur
     */
    public void setJoueurs(Joueur[] joueurs) { // met tous les joueurs sur la case
        ArrayList<Joueur> newJ = new ArrayList<Joueur>(Arrays.asList(joueurs));
        this.joueurs = newJ;
    }

    /**
     * Méthode qui met un seul joueur sur la case
     * 
     * @param j un joueur
     * @see Joueur
     * @see Case#getJoueurs()
     */
    public void addJoueur(Joueur j) { // met un seul joueur sur la case
        if (!this.getJoueurs().contains(j)) {
            this.getJoueurs().add(j);
        }
    }

    /**
     * Méthode qui supprime un joueur de la case
     * 
     * @param j un joueur à supprimer de la case
     * @see Case#getJoueurs()
     * @see Joueur
     */
    public void removeJoueur(Joueur j) {
        if (this.getJoueurs().contains(j)) {
            this.getJoueurs().remove(j);
        }
    }

    /**
     * Méthode qui renvoie une Arraylist de joueurs se trouvant sur la case
     * 
     * @return une Arraylist de joueurs se trouvant sur la case
     */

    public ArrayList<Joueur> getJoueurs() { // tous les joueurs
        return this.joueurs;
    }

    /**
     * Méthode qui renvoie la représentation des joueurs de la case
     * 
     * @param nbJ la nombre de joueurs de la case
     * @return la représentation des joueurs de la case
     * @see Case#getJoueurs()
     * @see Joueur
     * @see Joueur#getPlayerId()
     */

    public String printJoueurs(int nbJ) {
        /**
         * fait un string avec les joueurs attribués à la case et laisse des espaces
         * pour les joueurs qui ne sont pas sur la case
         */
        String all = "";
        if (this.getJoueurs() != null) {
            int nbJcase = this.getJoueurs().size();
            for (int i = 0; i < nbJcase; i++) {
                all = all + " " + "J" + String.valueOf(this.getJoueurs().get(i).getPlayerId() + 1);
            }
            if (nbJcase < nbJ) {
                int diff = nbJ - nbJcase;
                for (int i = 0; i < diff; i++) {
                    all = all + "   ";
                }
            }
        } else {
            for (int i = 0; i < nbJ; i++) {
                all = all + "   ";
            }
        }
        return all;
    }

    /**
     * Méthode abstraite qui renvoie la représentation de la case
     * 
     * @return la représentation de la case
     */
    public abstract String toString();

    /**
     * Méthode abstraite qui renvoie un booléen indiquant si la case est finale
     * 
     * @return un booléen indiquant si la case est finale
     */

    public abstract boolean isFinale();

    /**
     * Méthode abstraite qui applique la logique correspondante en fonction du type
     * de la case
     * 
     * @param p le plateau
     * @param j le joueur
     */

    public abstract void effect(Plateau p, Joueur j);

    /**
     * Méthode abstraite qui renvoie la représentation de la case
     * 
     * @return la représentation de la case
     */
    public abstract String printTypeCLI();
}
