package jeu.plateau.cases;

import jeu.Joueur;
import jeu.plateau.Plateau;

/**
 * Classe représentant la case normale du plateau
 * @inheritDoc Case
 * @see Case
 */

public class CaseNormale extends Case {

    /**
     * Méthode qui renvoie la représentation de la case
     * 
     * @return la représentation textuelle de la case
     */
    @Override
    public String toString() {
        return "Normale";
    }

    /**
     * Méthode qui renvoie un booléen indiquant si c'est la dernière case du plateau
     * 
     * @return false car la case de type normal ne peut pas être finale
     */

    @Override
    public boolean isFinale() {
        return false;
    }

    @Override
    public void effect(Plateau p, Joueur j) {

    }

    /**
     * Méthode qui renvoie la représentation de la case pour l'impression du plateau
     * 
     * @return la représentation du type de la case
     */
    @Override
    public String printTypeCLI() {
        return " ";
    }
}
