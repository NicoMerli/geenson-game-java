package jeu.plateau.cases;

import jeu.Joueur;
import jeu.plateau.Plateau;

/**
 * Classe représentant la case qui fait reculer le joueur sur le plateau
 * @inheritDoc Case
 * @see Case
 */

public class CaseRecule extends Case {

	/**
	 * Méthode qui renvoie la représentation textuelle de la case
	 * 
	 * @return la représentation textuelle de la case
	 */
	@Override
	public String toString() {
		return "Recule";
	}

	/**
	 * Méthode qui renvoie un booléen indiquant si c'est la dernière case du plateau
	 * 
	 * @return false car la case de type Recule ne peut pas être finale
	 */

	@Override
	public boolean isFinale() {
		return false;
	}

	/**
	 * Méthode qui fait avancer le joueur sur le plateau
	 * 
	 * @param p le plateau
	 * @param j le joueur
	 * @see Plateau
	 * @see Plateau#getCases()
	 * @see Case#getCaseId()
	 * @see Joueur#getCase()
	 * @see Case#addJoueur(Joueur)
	 */
	@Override
	public void effect(Plateau p, Joueur j) {
		j.setCase(p.getCases()[j.getCase().getCaseId() - 3]); // attribution de la nouvelle case au joueur
		p.getCases()[j.getCase().getCaseId() + 3].removeJoueur(j); // on enlève le joueur de la case précédente
		j.getCase().addJoueur(j); // attribution du joueur à la case
	}

	@Override
	public String printTypeCLI() {
		return "<";
	}
}
