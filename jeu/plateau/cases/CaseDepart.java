package jeu.plateau.cases;

import jeu.Joueur;
import jeu.plateau.Plateau;

    /**
     *Classe représentant la case initiale du plateau
     *@see Case
     *@inheritDoc Case
     */

public class CaseDepart extends Case {

    /**
     *Méthode qui renvoie la représentation de la case
     *@return la représentation textuelle de la case
     */
    @Override
    public String toString() {
        return "Départ";
    }

    /**
     *Méthode qui renvoie un booléen indiquant si la case est finale
     *@return false car la case est initiale
     */
    @Override
    public boolean isFinale() {
        return false;
    }

    @Override
    public void effect(Plateau p, Joueur j) {
    }

    /**
      *Méthode qui renvoie la représentation de la case pour l'impression du plateau
      *@return la représentation du type de la case
      */
    @Override
    public String printTypeCLI() {
        return "@";
    }
}
