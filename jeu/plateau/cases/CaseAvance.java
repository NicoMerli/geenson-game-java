package jeu.plateau.cases;

import jeu.Joueur;
import jeu.plateau.Plateau;

/**
 * Classe représentant la case qui fait avancer le joueur sur le plateau
 * @inheritDoc Case
 * @see Case
 */
public class CaseAvance extends Case {
	/**
	 * Méthode qui renvoie la représentation textuelle de la case
	 * 
	 * @return la représentation textuelle de la case
	 */
	@Override
	public String toString() {
		return "Avance";
	}

	/**
	 * Méthode qui renvoie un booléen indiquant si c'est la dernière case du plateau
	 * 
	 * @return false car la case de type Avance ne peut pas être finale
	 */

	@Override
	public boolean isFinale() {
		return false;
	}

	/**
	 * Méthode qui fait avancer le joueur sur le plateau
	 * 
	 * @param p le plateau
	 * @param j le joueur
	 * @see Plateau
	 * @see Plateau#getCases()
	 * @see Case#getCaseId()
	 * @see Joueur#getCase()
	 * @see Case#addJoueur(Joueur)
	 */
	@Override
	public void effect(Plateau p, Joueur j) {
		j.setCase(p.getCases()[j.getCase().getCaseId() + 2]); // attribution de la nouvelle case au joueur
		p.getCases()[j.getCase().getCaseId() - 2].removeJoueur(j); // on enlève le joueur de la case précédente
		j.getCase().addJoueur(j); // attribution du joueur à la case
	}

	/**
	 * Méthode qui renvoie la représentation de la case pour l'impression du plateau
	 * 
	 * @return la représentation du type de la case
	 */

	@Override
	public String printTypeCLI() {
		return ">";
	}
}
