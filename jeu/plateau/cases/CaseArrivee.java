package jeu.plateau.cases;

import jeu.Joueur;
import jeu.plateau.Plateau;

    /**
     *Classe représentant la case finale du plateau
     *@inheritDoc Case
     *@see Case
     */
public class CaseArrivee extends Case {
    /**
     *Méthode qui renvoie la représentation de la case
     *@return la représentation textuelle de la case
     */
    @Override
    public String toString() {
        return "Arrivée";
    }
    /**
     *Méthode qui renvoie un booléen indiquant si la case est finale
     *@return true car la case est bien finale
     */
    public boolean isFinale() {
        return true;
    }
    
    @Override
    public void effect(Plateau p, Joueur j) {

    }

     /**
      *Méthode qui renvoie la représentation de la case pour l'impression du plateau
      *@return la représentation du type de la case
      */
    
    @Override
    public String printTypeCLI() {
        return "#";
    }
}
