package jeu.vecteurs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 *La classe qui crée un hashmap de mots et leurs vecteurs à partir du fichier
 */
public class VectorsMap extends HashMap<String, Vector> {

    protected String[] wordList;

    /**
     *Constructeur qui crée un hasmap de mots et leurs vecteurs à partir du fichier rentré en paramètre
     *@param filename le nom du fichier
     *@throws IOException
     */
    public VectorsMap(String filename) throws IOException {

        BufferedReader br = new BufferedReader(new FileReader(filename));
        String line = br.readLine();

        while (line != null) {
            if (!line.equals("") && !line.equals("51631 100")) {
                // System.out.println(line);
                String[] parts = line.split(" ");
                String label = parts[0];
                double[] values = new double[100];
                for (int i = 1; i < parts.length; i++) {
                    values[i - 1] = Double.parseDouble(parts[i]);
                }
                Vector v = new Vector(label, values);
                super.put(v.label, v);
                line = br.readLine();
            } else {
                line = br.readLine();
            }
        }

        wordList = this.keySet().toArray(new String[this.keySet().size()]);
        br.close();
    }

    /**
     *Méthode qui renvoie un string représentant la taille du vectorsmap
     */
    public String toString() {
        return "Map de " + this.size() + " vecteurs";
    }

    /**
     *Méthode qui renvoie la liste de mots contenus dans le vectorsmap
     *@return la liste de mots contenus dans le vectorsmap
     */
    public String[] getWords() {
        return this.wordList;
    }
}
