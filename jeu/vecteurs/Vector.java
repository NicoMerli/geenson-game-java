
package jeu.vecteurs;

    /**
     *Classe représentant le vecteur des mots
     */

public class Vector {

    /**
     *Le label du mot
     */
    protected String label;
    /**
     *Un tableau de valeurs du mot
     */
    protected double[] values;

    /**
     *Constructeur de la classe
     *@param label le label du mot
     *@param values un tableau de valeurs du mot
     */
    public Vector(String label, double[] values) {
        this.label = label;
        this.values = values;
    }

    /**
     *Constructeur de la classe
     *@param values un tableau de valeurs
     */
    public Vector(double[] values) {
        this.values = values;
        this.label = null;
    }

    /**
     *La méthode qui renvoie la représentation textuelle du mot et de ses valeurs
     *@return la représentation textuelle du mot et de ses valeurs
     */
    public String toString() {
        String s=this.label + ": ";
	for (int i=0;i<this.values.length;i++){
	    s=s+String.valueOf(this.values[i])+";";
	}
	return s;
    }

    /**
     *Méthode qui met à jour le label
     *@param label un string de label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     *Méthode qui met à jour le tableau de valeurs
     *@param values le tableau de valeurs
     */
    
    public void setValues(double[] values) {
        this.values = values;
    }

    /**
     *Méthode qui renvoie le label
     *@return le label
     */
    public String getLabel() {
        return this.label;
    }

    /**
     *Méthode qui renvoie le tableau de valeurs
     *@return le tableau de valeurs
     */
    public double[] getValues() {
        return this.values;
    }

    /**
     *Méthode qui calcule l'addition des vecteurs
     *@param v représentant le vecteur à additionner
     *@return vecteur résultant de l'addition des vecteurs
     */
    public Vector add(Vector v) {
	double[] res = new double[100];
        Vector v1=new Vector (res);
        for (int i = 0; i < this.values.length; i++) {
            v1.getValues()[i] = this.values[i] + v.getValues()[i];
        }
        return v1;
    }

    /**
     *Méthode qui calcule la similarité cosinus entre deux vecteurs
     *@param v vecteur avec lequel on veut calculer la similarité
     *@return la similarité (double)
     */
    public double cosSimilarity(Vector v) {
        return this.scalarProduct(v) / (this.norm() * v.norm());
    }

    /**
     *Méthode qui calcule la soustraction des vecteurs
     *@param v représentant le vecteur à soustraire
     *@return vecteur résultant de la soustraction des vecteurs
     */
    public Vector substract(Vector v) {
        double[] res = new double[100];
        Vector v1=new Vector (res);
        for (int i = 0; i < this.values.length; i++) {
            res[i] = this.values[i] - v.getValues()[i];
        }
        return v1;
    }


    /**
     *Méthode qui calcule la moyenne de vecteurs
     *@param v premier vecteur
     *@param v2 deuxième vecteur
     *@return le vecteur représentant la moyenne de deux vecteurs
     */
    public Vector moyenne(Vector v, Vector v2){
	Vector Vfinal= new Vector(this.values);
	Vfinal=Vfinal.add(v);
	Vfinal=Vfinal.add(v2);
	
	for (int i=0;i<Vfinal.values.length;i++){
	    Vfinal.values[i]=Vfinal.values[i]/3;
	}
	return Vfinal;
    }

    /**
     *Méthode qui calcule le produit scalaire des vecteurs
     *@param v vecteur
     *@return le produit scalaire (double)
     */
    public double scalarProduct(Vector v) {
        double res = 0;
        for (int i = 0; i < this.values.length; i++) {
            res += v.values[i] * this.values[i];
        }
        return res;
    }

    /**
     *Méthode qui renvoie le produit de vecteurs
     *@param v vecteur
     *@return le vecteur résultant du produit de vecteurs
     */
    public double[] vectorProduct(Vector v) {
        double[] res = new double[100];
        for (int i = 0; i < this.values.length; i++) {
            res[i] = this.values[i] * v.values[i];
        }
        return res;
    }

    /**
     *Méthode qui calcule la distance entre vecteurs
     *@param v vecteur
     *@return la distance entre vecteurs (double)
     */
    public double distance(Vector v) {
        double somme = 0;
        for (int i = 0; i < this.values.length; i++) {
            somme += Math.pow(this.values[i] - v.values[i], 2);
        }
        return Math.sqrt(somme);
    }

    /**
     *Méthode qui calcule la norme du vecteur
     *@return la norme du vecteur
     */
    public double norm() {
        double somme = 0;
        for (int i = 0; i < this.values.length; i++) {
            somme += Math.pow(this.values[i], 2);
        }
        return Math.sqrt(somme);
    }


}
